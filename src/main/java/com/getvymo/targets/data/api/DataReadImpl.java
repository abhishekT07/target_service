package com.getvymo.targets.data.api;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getvymo.achievement.GetAchievement;
import com.getvymo.achievement.GetAchievementImpl;
import com.getvymo.data.response.entities.Metadata;
import com.getvymo.cache.facts.FactsCacheImpl;
import com.getvymo.cache.facts.IFactsCache;
import com.getvymo.data.response.entities.AchievementUserEntity;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.*;

import com.getvymo.cache.IDateKeyCache;
import com.getvymo.data.response.entities.ActiveTargets;
import com.getvymo.targets.spi.IDataReadSpi;

import org.springframework.jdbc.core.JdbcTemplate;

@Component
// @Configurable
public class DataReadImpl implements IDataReadSpi {

	/*
	 * @Autowired DateKeyFactory dateFactory;
	 */

	@Autowired
	IDateKeyCache dateKeyCache;

	@Autowired
	JdbcTemplate jdbcTemplate;

//	private String GET_ALL_TARGETS_SELF = "select tins.label as name, tins.id as instanceId, tins.def_id as factDefId, m.id as metricFactId, m.fact_id as metricId "
//			+ ",u.value as value, "
//			+ "u.user_code as userId,tins.achievement_type as targetType,tins.description as description,u.date_key as dateKey,m.value as metricValue,m.fact_dimension as factDimension  from "
//			+ " targets_service.user_vs_target u join targets_service.metric_fact m on u.metric_fact_id=m.id join targets_service.target_instance tins on tins.id =u.instance_id  "
//			+ " where u.user_code= '%s' and u.client_code='%s'"
//			+ " and u.date_key in %s"
//			+ " and tins.client_code='%s'  and  m.client_code='%s'"
//			+ "group by tins.label,tins.id , tins.def_id ,m.id, m.fact_id,m.value,u.value,u.date_key,tins.description,u.user_code,u.hierarchy_key,m.fact_dimension,m.value,tins.achievement_type,u.date_key,m.value,m.fact_dimension";

	private String GET_ALL_TARGETS_SELF = "select sum(uvt.value) as value ,ti.label as name,ti.id as instanceId,  ti.def_id as factDefId,mf.id as metricFactId, "
			+ "mf.fact_id as metricId, mf.value as metricValue,ti.description, "
			+ " mf.fact_dimension as factDimension,ti.achievement_type as targetType,uvt.date_key as dateKey,uvahk.userId as userId "
			+ "from  targets_service.user_vs_target uvt join targets_service.target_instance ti on uvt.instance_id=ti.id join targets_service.metric_fact mf on mf.id=uvt.metric_fact_id join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on uvt.hierarchy_key=uvahk.hierarchyKey"
			+ " where  " + "uvahk.userId='%s' and uvahk.clientId='%s' and uvahk.own=1 "
			+ " " +
			"and uvt.date_key in %s"
			+ "  and uvt.client_code='%s' and  mf.client_code='%s' and uvahk.clientId='%s' and uvt.disabled=0 group by ti.id ";

	private String GET_ALL_TARGETS_TEAM = "select sum(uvt.value) as value ,ti.label as name,ti.id as instanceId,  ti.def_id as factDefId,mf.id as metricFactId, "
			+ "mf.fact_id as metricId, mf.value as metricValue,ti.description, "
			+ " mf.fact_dimension as factDimension,ti.achievement_type as targetType,uvt.date_key as dateKey,uvahk.userId as userId "
			+ "from  targets_service.user_vs_target uvt join targets_service.target_instance ti on uvt.instance_id=ti.id join targets_service.metric_fact mf on mf.id=uvt.metric_fact_id join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on uvt.hierarchy_key=uvahk.hierarchyKey"
			+ " where  " + "uvahk.userId='%s' and uvahk.clientId='%s'"
			+ " " +
			"and uvt.date_key in %s"
			+ "  and uvt.client_code='%s' and  mf.client_code='%s' and uvahk.clientId='%s' and uvt.disabled=0 group by ti.id ";

	private String GET_TARGETS_BY_USER = "SELECT "+
			"SUM(uvt.value) AS value,"+
			"ti.label AS name,"+
			"ti.id AS instanceId,"+
			"ti.def_id AS factDefId,"+
			"mf.id AS metricFactId,"+
			"mf.fact_id AS metricId,"+
			"mf.value AS metricValue,"+
			"ti.description,"+
			"mf.fact_dimension AS factDimension,"+
			"ti.achievement_type AS targetType,"+
			"uvt.date_key AS dateKey,"+
			"user_code AS user_id"+
			"		FROM"+
			" targets_service.user_vs_target uvt"+
			" JOIN"+
			" targets_service.target_instance ti ON uvt.instance_id = ti.id"+
			"		JOIN"+
			" targets_service.metric_fact mf ON mf.id = uvt.metric_fact_id"+
			"		JOIN"+
			" org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk ON uvt.hierarchy_key = uvahk.hierarchyKey"+
			"		WHERE"+
			" uvahk.userId = '%s'"+
			" AND uvahk.clientId = '%s'"+
			" AND uvt.date_key IN %s"+
			" AND uvt.client_code = '%s'"+
			" AND mf.client_code = '%s'"+
			" AND uvahk.clientId = '%s'"+
			" AND uvt.metric_fact_id='%s'"+
			" AND mf.id='%s'"+
			" AND ti.def_id='%s'"+
			" AND ti.achievement_type='%s'" +
			" AND uvt.disabled=0 "+
			" GROUP BY user_code";

	private String GET_ALL_TARGETS_SUMMARY_BY_DATE = "select sum(uvt.value) as value ,ti.label as name,ti.id as instanceId,  ti.def_id as factDefId,mf.id as metricFactId, "
			+ "mf.fact_id as metricId, mf.value as metricValue,ti.description, "
			+ " mf.fact_dimension as factDimension,ti.achievement_type as achievement_type,uvt.date_key as dateKey,uvahk.userId as userId "
			+ "from  targets_service.user_vs_target uvt join targets_service.target_instance ti on uvt.instance_id=ti.id join targets_service.metric_fact mf on mf.id=uvt.metric_fact_id join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on uvt.hierarchy_key=uvahk.hierarchyKey"
			+ " where  " + "uvahk.userId='%s' and uvahk.clientId='%s'"
			+ " " +
			"and uvt.date_key in %s"
			+ "  and uvt.client_code='%s' and  mf.client_code='%s' and uvahk.clientId='%s' and uvt.disabled=0 group by ti.def_id,ti.metric_fact_id,ti.achievement_type ";

	private String GET_ALL_TARGETS_SUMMARY = "select sum(uvt.value) as value ,ti.label as name,ti.id as instanceId,  ti.def_id as factDefId,mf.id as metricFactId, "
			+ "mf.fact_id as metricId, mf.value as metricValue,ti.description, "
			+ " mf.fact_dimension as factDimension,ti.achievement_type as achievement_type,uvt.date_key as dateKey,uvahk.userId as userId "
			+ "from  targets_service.user_vs_target uvt join targets_service.target_instance ti on uvt.instance_id=ti.id join targets_service.metric_fact mf on mf.id=uvt.metric_fact_id join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on uvt.hierarchy_key=uvahk.hierarchyKey"
			+ " where  " + "uvahk.userId='%s' and uvahk.clientId='%s'"
			+ " "
			+ "  and uvt.client_code='%s' and  mf.client_code='%s' and uvahk.clientId='%s' and uvt.disabled=0 group by ti.def_id,ti.metric_fact_id,ti.achievement_type ";




	private String GET_ALL_USERS_TARGETS_TEAM = "select distinct uvt.user_code from targets_service.user_vs_target uvt," +
			" org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk where uvahk.userId = '%s' and uvahk.clientId='%s' " +
			"and uvahk.clientId=uvt.client_code and uvahk.hierarchyKey=uvt.hierarchy_key and uvt.date_key in %s";


	private static org.slf4j.Logger logger = LoggerFactory.getLogger(DataReadImpl.class);
	public final static long MILLISECONDS_IN_DAY = 24 * 60 * 60 * 1000;
	// DateKeyFactory dateFactory = new DateKeyFactory();
	// IDateKeyCache targets = dateFactory.getDateKeyCache();

	public List<ActiveTargets> listActiveTargetsForUser(String userId, String clientId, String self) {
		logger.info("find by user code called");
		 GetAchievement getAchievement = new GetAchievementImpl();
		IFactsCache iFactsCache = new FactsCacheImpl();
		Date sqlDate = new Date(System.currentTimeMillis());
		String jsonString = null;
		String description = null;
		List<String> listdates = dateKeyCache.getLiveKeys(clientId,sqlDate);
		if(listdates==null){
			logger.error("Failed to get date keys user -> "+userId+" clientId "+clientId);
			return null;
		}
		//HashMap<String, List<Date>> resultMap = dateKeyCache.getStartAndEndDate();
		
		List<ActiveTargets> activeAllTargets = new ArrayList<ActiveTargets>();
		List<ActiveTargets> resultTargets = new ArrayList<ActiveTargets>();

		List<ActiveTargets> result = null;
		String temp = getInQueryFromList(listdates);


		if (self.equals("true")) {

			String getAllTargets = String.format(GET_ALL_TARGETS_SELF,userId,clientId,temp,clientId,clientId,clientId);
			logger.debug("Get all targets -> "+getAllTargets);
			logger.info("get all the target instances for a user ");
			Object[] params = listdates.toArray();
			activeAllTargets = jdbcTemplate.query(getAllTargets, params,
					new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));

			for (ActiveTargets target : activeAllTargets) {
				HashMap<String, String> metricMap = new HashMap<String, String>();
				String dateKey = target.getDateKey();
				List<Date> startAndEndList = dateKeyCache.getStartAndEndDate(clientId,dateKey);
				//logger.info("Date from map today"+resultMap.get(target.getDateKey()).get(1)+"..."+sqlDate);

				//if (((sqlDate).after(resultMap.get(target.getDateKey()).get(1)))) {
					

					//if (resultMap.containsKey(target.getDateKey())) {
						logger.info("start and end date map");

						target.setStartDate(startAndEndList.get(0).getTime());
						target.setEndDate(startAndEndList.get(1).getTime() + 86399999);

						target.setUserId(userId);
						target.setClientId(clientId);
						metricMap.put(target.getFactDimension(), target.getMetricValue());
						target.setMetricFact(metricMap);
						Metadata metadata = iFactsCache.getFactMetadata(target.getMetricFactId());
						target.setMetadata(metadata);
				        target.setAchieved(getAchievement.getAchievement(target.getMetricFactId(), userId, self,
						startAndEndList.get(0).toString(), startAndEndList.get(1).toString(), clientId, target.getMetricId(), null));
						target.setTargetType(iFactsCache.getTargetType(target.getMetricId()));
						target.setActive(true);
						resultTargets.add(target);
					//}
					
				//}

			}

		} else {

			logger.info("find all users under a manager-method called");

			logger.info("list of active target instances group by instance Id");
			String getAllTargets =  String.format(GET_ALL_TARGETS_TEAM,userId,clientId,temp,clientId,clientId,clientId);
			logger.debug("Get all targets -> "+getAllTargets);
			String getAllUsersForAchievement = String.format(GET_ALL_USERS_TARGETS_TEAM, userId, clientId, temp);

			
			Object[] params = listdates.toArray();
			
			List<ActiveTargets> activeTargets = jdbcTemplate.query(getAllTargets, params,
					new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));
			List<AchievementUserEntity> achievementUsers = jdbcTemplate.query(getAllUsersForAchievement,params, new BeanPropertyRowMapper<AchievementUserEntity>(AchievementUserEntity.class));

//			logger.info("GOT USER LIST"+achievementUsers.get(0).getUser_code());

			for (ActiveTargets target : activeTargets) {
				HashMap<String, String> metricMap = new HashMap<String, String>();
				String dateKey = target.getDateKey();
				List<Date> startAndEndList = dateKeyCache.getStartAndEndDate(clientId,dateKey);
				//if ((sqlDate).after(resultMap.get(target.getDateKey()).get(1)) ) {

				//	if (resultMap.containsKey(target.getDateKey())) {
						logger.info("start and end date map");
						target.setStartDate(startAndEndList.get(0).getTime());
						target.setEndDate(startAndEndList.get(1).getTime() + 86399999);
						target.setUserId(userId);
						target.setClientId(clientId);
						metricMap.put(target.getFactDimension(), target.getMetricValue());
						target.setMetricFact(metricMap);
						target.setActive(true);
						target.setAchieved(getAchievement.getAchievement(target.getMetricFactId(), null, self,
						startAndEndList.get(0).toString(), startAndEndList.get(1).toString(), clientId, target.getMetricId(), achievementUsers));
						Metadata metadata = iFactsCache.getFactMetadata(target.getMetricFactId());
						target.setMetadata(metadata);
						target.setTargetType(iFactsCache.getTargetType(target.getMetricId()));
						resultTargets.add(target);
				//	}
					
				//}

			}

		}

		if (resultTargets.size() >= 2) {
			resultTargets = jsonResultMapping(resultTargets);
		}
		return resultTargets;

	}

	public List<ActiveTargets> listTargetsByDate(String userId, String self, String clientId, Date startDate,
												 Date endDate) {
		logger.info("find by user code called");
		return null;
//		Date date = new Date(System.currentTimeMillis());
//		String jsonString = null;
//		String description = null;
//
//		List<String> dateKeys = dateKeyCache.getStartAndEndDate(clientId,date.toString());
//		List<ActiveTargets> activeAllTargets = new ArrayList<ActiveTargets>();
//		List<ActiveTargets> resultTargets = new ArrayList<ActiveTargets>();
//
//		for (Entry<String, List<Date>> entry : resultMap.entrySet()) {
//			System.out.println(
//					"keys today" + entry.getKey() + ".." + entry.getValue().get(0) + ".." + entry.getValue().get(1));
//		}
//
//		List<ActiveTargets> result = null;
//
//		String temp = "(";
//		for (String values : listdates) {
//			temp += "?,";
//		}
//		temp += ")";
//		temp = temp.replace(",)", ")");
//
//		if (self.equals("true")) {
//
//			String getAllTargets = "select tins.label as name, tins.id as instanceId, tins.def_id as factDefId, m.id as metricId, m.fact_id as metricFactId "
//					+ ",m.value as metricValue,u.value as value,  "
//					+ "tins.description as description,m.fact_dimension as factDimension,tins.achievement_type as targetType,u.date_key as dateKey  from  targets_service.user_vs_target u join targets_service.metric_fact m on mf.id=uvt.metric_fact_id join "
//					+ "targets_service.target_instance tins on tins.id =u.instance_id  " + " where u.user_code='"
//					+ userId + "' and  u.client_code='" + clientId + "' and u.date_key in " + temp
//					+ "   and  u.client_code=tins.client_code  and m.client_code=u.client_code "
//					+ "group by tins.label,tins.id , tins.def_id ,m.id, m.fact_id,m.value,u.value,u.date_key,tins.description,m.fact_dimension,m.value,tins.achievement_type";
//
//			logger.info("get all the target instances for a user ");
//
//			Object[] params = listdates.toArray();
//			List<ActiveTargets> activeTargets = jdbcTemplate.query(getAllTargets, params,
//					new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));
//
//			for (ActiveTargets target : activeTargets) {
//				HashMap<String, String> metricMap = new HashMap<String, String>();
//
//				if (resultMap.containsKey(target.getDateKey())
//						&& sqlDate.after(resultMap.get(target.getDateKey()).get(0))
//						&& sqlDate.before(resultMap.get(target.getDateKey()).get(1))) {
//					logger.info("start and end date map");
//					target.setStartDate(resultMap.get(target.getDateKey()).get(0).getTime());
//					target.setEndDate(resultMap.get(target.getDateKey()).get(1).getTime());
//					target.setUserId(userId);
//					target.setClientId(clientId);
//					metricMap.put(target.getFactDimension(), target.getMetricValue());
//					target.setMetricFact(metricMap);
//					int daysRemaining = (int) ((endDate.getTime() - sqlDate.getTime()) / MILLISECONDS_IN_DAY);
//					if ((daysRemaining + 1) <= 0) {
//						target.setActive(false);
//
//					} else {
//
//						target.setActive(true);
//					}
//					resultTargets.add(target);
//				}
//
//			}
//
//		}
//
//		else {
//			String getAllTargets = "select distinct uvahk.clientId as clientId,ti.label as name, ti.id as instanceId , ti.def_id as factDefId,mf.id as metricId, mf.fact_id as metricFactId, mf.value as metricValue,uvt.value as value,ti.description ,  "
//					+ " uvt.user_code as userId,mf.fact_dimension as factDimension,ti.achievement_type as targetType,uvt.date_key as dateKey from "
//					+ "targets_service.user_vs_target uvt join targets_service.target_instance ti on uvt.instance_id=ti.id join targets_service.metric_fact mf on mf.id=uvt.metric_fact_id  join  "
//					+ "  org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on uvt.hierarchy_key=uvahk.hierarchyKey where uvt.date_key in "+temp+" and uvahk.userId='"
//					+ userId + "' and uvahk.clientId='" + clientId + "' and "
//					+ "uvt.client_code=ti.client_code and uvt.client_code=td.client_code and mf.client_code=uvt.client_code and uvahk.clientId=uvt.client_code ";
//
//			logger.info("get all the target instances with the given date range");
//
//			Object[] params = listdates.toArray();
//			List<ActiveTargets> activeTargets = jdbcTemplate.query(getAllTargets, params,
//					new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));
//
//			for (ActiveTargets target : activeTargets) {
//				HashMap<String, String> metricMap = new HashMap<String, String>();
//
//				if (resultMap.containsKey(target.getDateKey())
//						&& sqlDate.after(resultMap.get(target.getDateKey()).get(0))
//						&& sqlDate.before(resultMap.get(target.getDateKey()).get(1))) {
//					logger.info("start and end date map");
//					target.setStartDate(resultMap.get(target.getDateKey()).get(0).getTime());
//					target.setEndDate(resultMap.get(target.getDateKey()).get(1).getTime());
//					target.setUserId(userId);
//					target.setClientId(clientId);
//					metricMap.put(target.getFactDimension(), target.getMetricValue());
//					target.setMetricFact(metricMap);
//					int daysRemaining = (int) ((endDate.getTime() - sqlDate.getTime()) / MILLISECONDS_IN_DAY);
//					if ((daysRemaining + 1) <= 0) {
//						target.setActive(false);
//
//					} else {
//
//						target.setActive(true);
//					}
//					resultTargets.add(target);
//				}
//
//			}
//
//		}
//
//		if (resultTargets.size() >= 2) {
//			resultTargets = jsonResultMapping(resultTargets);
//		}
//		return resultTargets;

	}

	/// Detailed view
	public List<ActiveTargets> getTargetDetailsByUser(String userId, String clientId, String targetDefinitionId,
													  String metricFactId, String achievementType, Date startDate, Date endDate) {
		//return null;

		IFactsCache iFactsCache = new FactsCacheImpl();
		GetAchievement getAchievement = new GetAchievementImpl();
		Date sqlDate = new Date(System.currentTimeMillis());
		String jsonString = null;
		String description = null;
		logger.info("Detailed view- method called");
		List<ActiveTargets> resultTargets = new ArrayList<ActiveTargets>();
		List<String> listdates = dateKeyCache.getLiveKeys(clientId,sqlDate);

		if(startDate!=null && endDate!=null){
			listdates = dateKeyCache.getKeysForDateRange(clientId,startDate,endDate);
		}

		if(listdates==null || listdates.size()==0){
			logger.info("No date keys found !");
			return resultTargets;
		}

		String temp = getInQueryFromList(listdates);


		String getAllTargets = String.format(GET_TARGETS_BY_USER,userId,clientId,temp,clientId,clientId,clientId,metricFactId,metricFactId,targetDefinitionId,achievementType);
		logger.debug("Targets details query -> "+getAllTargets);
		Object[] params = listdates.toArray();
		//Making Achievement call
		String getAllUsersForAchievement = String.format(GET_ALL_USERS_TARGETS_TEAM, userId, clientId, temp);
		List<AchievementUserEntity> achievementUsers = jdbcTemplate.query(getAllUsersForAchievement, params,
				new BeanPropertyRowMapper<AchievementUserEntity>(AchievementUserEntity.class));


		List<ActiveTargets> activeTargets = jdbcTemplate.query(getAllTargets, params,
				new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));
		List<Date> dateList = null;
		for (ActiveTargets target : activeTargets) {
			HashMap<String, String> metricMap = new HashMap<String, String>();
			String dateKey = target.getDateKey();
			List<Date> startAndEndList = dateKeyCache.getStartAndEndDate(clientId,dateKey);
			dateList = startAndEndList;
			//if (sqlDate.after(resultMap.get(target.getDateKey()).get(1))) {
				logger.info("start and end date map");
				target.setStartDate(startAndEndList.get(0).getTime());
				target.setEndDate(startAndEndList.get(1).getTime()+86399999);
				//target.setUserId(userId);
				target.setClientId(clientId);
				metricMap.put(target.getFactDimension(), target.getMetricValue());
				target.setMetricFact(metricMap);
				target.setActive(true);
				target.setAchieved(0);
				target.setTargetType(iFactsCache.getTargetType(target.getMetricId()));
				Metadata metadata = iFactsCache.getFactMetadata(target.getMetricFactId());
				target.setMetadata(metadata);
				resultTargets.add(target);
			//}

		}
		if (activeTargets.size()>0){
			String start,end;
			if (startDate!=null && endDate!=null){
				start= startDate.toString();
				end = endDate.toString();
			} else {
				start = dateList.get(0).toString();
				end = dateList.get(1).toString();
			}
			JSONObject achievementObject = getAchievement.getAchievement(Integer.parseInt(metricFactId),
					start, end, clientId, achievementUsers);
			if(achievementObject!=null){
				JSONObject userObjects = (JSONObject) achievementObject.get("value");
				logger.info("ACHIEVEMENT JSON OBJECT ++++++++"+achievementObject.get("value")+userObjects.keySet());
				for (ActiveTargets currentTarget: resultTargets){
					if(userObjects.containsKey(currentTarget.getUserId())){
						currentTarget.setAchieved(Float.parseFloat(userObjects.get(currentTarget.getUserId())+""));
					} else {
						currentTarget.setAchieved(0);
					}
				}
			}

		}



//		 if (resultTargets.size() >= 2) {
//		 resultTargets = jsonResultMapping(resultTargets);
//		 }
		return resultTargets;
	}

	public List<ActiveTargets> jsonResultMapping(List<ActiveTargets> ActiveTargets) {
		logger.info("Metric value - function called");
		HashMap<Integer, HashMap<String, String>> factMap = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> intermediateMap = new HashMap<String, String>();
		Set<ActiveTargets> resultActiveTargets = new HashSet<ActiveTargets>();
		List<ActiveTargets> activeTargetsList = new ArrayList<ActiveTargets>();

		for (int i = 0; i < ActiveTargets.size(); i++) {
			for (int j = i + 1; j < ActiveTargets.size(); j++) {

				if (ActiveTargets.get(i).getInstanceId().equals(ActiveTargets.get(j).getInstanceId())
						&& ActiveTargets.get(i).getMetricId() == ActiveTargets.get(j).getMetricId()) {

					intermediateMap.putAll(ActiveTargets.get(j).getMetricFact());

					intermediateMap.putAll(ActiveTargets.get(i).getMetricFact());
					factMap.put(ActiveTargets.get(i).getMetricFactId(), intermediateMap);
					ActiveTargets.get(i).setMetricFact(factMap.get(ActiveTargets.get(i).getMetricId()));

				}

			}

			resultActiveTargets.add(ActiveTargets.get(i));

		}
		activeTargetsList.addAll(resultActiveTargets);
		return activeTargetsList;

	}

	public List<ActiveTargets> getTargetsSummary(String userId, String self, String clientId, Date startDate,
												 Date endDate){
		logger.info("find by user code called");
		GetAchievement getAchievement = new GetAchievementImpl();
		Date sqlDate = new Date(System.currentTimeMillis());
		String jsonString = null;
		String description = null;
		List<String> listdates = null;

		if(startDate!=null && endDate!=null){
			listdates = dateKeyCache.getKeysForDateRange(clientId,startDate,endDate);
		}



		List<ActiveTargets> activeAllTargets = new ArrayList<ActiveTargets>();
		List<ActiveTargets> resultTargets = new ArrayList<ActiveTargets>();

		List<ActiveTargets> result = null;
		String getTargetsSummary = null;
		Object[] params = new Object[0];
		if(listdates!=null&&listdates.size()>0){
			String temp = getInQueryFromList(listdates);
			getTargetsSummary =  String.format(GET_ALL_TARGETS_SUMMARY_BY_DATE,userId,clientId,temp,clientId,clientId,clientId);
			params = listdates.toArray();
		} else {
			getTargetsSummary =  String.format(GET_ALL_TARGETS_SUMMARY,userId,clientId,clientId,clientId,clientId);
		}

		logger.debug("Targets summary query -> "+getTargetsSummary);


		List<ActiveTargets> activeTargets = jdbcTemplate.query(getTargetsSummary, params,
				new BeanPropertyRowMapper<ActiveTargets>(ActiveTargets.class));

		for (ActiveTargets target : activeTargets) {
			HashMap<String, String> metricMap = new HashMap<String, String>();
			String dateKey = target.getDateKey();
			List<Date> startAndEndList = dateKeyCache.getStartAndEndDate(clientId,dateKey);
			//if ((sqlDate).after(resultMap.get(target.getDateKey()).get(1)) ) {

			//	if (resultMap.containsKey(target.getDateKey())) {
			logger.info("start and end date map");
			target.setStartDate(startAndEndList.get(0).getTime());
			target.setEndDate(startAndEndList.get(1).getTime());
			target.setUserId(userId);
			target.setClientId(clientId);
			metricMap.put(target.getFactDimension(), target.getMetricValue());
			target.setMetricFact(metricMap);
			target.setActive(true);
			target.setAchieved(0);

		}
		return activeTargets;
	}

	private String getInQueryFromList(List<String> list){
		String temp = "(";
		for (String values : list) {
			temp += "?,";
		}
		temp += ")";
		temp = temp.replace(",)", ")");
		return temp;
	}

}
