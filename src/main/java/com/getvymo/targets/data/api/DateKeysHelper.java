package com.getvymo.targets.data.api;

import com.getvymo.cache.IDateKeyCache;
import com.getvymo.cache.impl.DateKeyCacheImpl;
import com.getvymo.data.sql.entities.target_entities.DateKeys;
import com.getvymo.data.sql.repositories.target_repos.TargetRepositoryDate;
import com.getvymo.exceptions.ExcelRowException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.List;
import com.getvymo.data.sql.repositoryHelper.Util;
import org.springframework.stereotype.Component;

/**
 * Created by abhishek on 03/04/17.
 */
@Component
public class DateKeysHelper {
    @Autowired
    IDateKeyCache dateKeyCache ;
    private static Logger logger = LoggerFactory.getLogger(DateKeysHelper.class);
    public String getNextDateKey(int stepForwardNumber, String period, TargetRepositoryDate targetRepositoryDate,
                                        String startDateString,String clientId){
        LocalDate localDate=null;
        DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        try {
            if (startDateString!=null || !startDateString.isEmpty()){
                java.util.Date tmp = dateFormat.parse(startDateString);
                localDate = new Date(tmp.getTime()).toLocalDate();
            } else {
                Date currentDate = new Date(System.currentTimeMillis());
                localDate = currentDate.toLocalDate();
            }
        } catch (Exception e){
            logger.info("Exception occured while parsing date "+e.getMessage());
            e.printStackTrace();
        }


        if (period.equals("day")){
            localDate = localDate.plusDays(stepForwardNumber);
        } else if (period.equals("week")){
            localDate = localDate.plusWeeks(stepForwardNumber);
        } else if (period.equals("month")){
            localDate = localDate.plusMonths(stepForwardNumber);
        } else if (period.equals("quarter")){
            localDate = localDate.plusMonths(3*stepForwardNumber);
        } else if (period.equals("year")){
            localDate = localDate.plusYears(stepForwardNumber);
        }
        Date newDate = Date.valueOf(localDate);
        List<String> keys = dateKeyCache.getLiveKeys(clientId,newDate);
        int index = Util.getIndexOfDateKey(period);
        if(index==-1){
            logger.error("Improper period passssed "+index);
        }
        //DateKeys newDateKeys = targetRepositoryDate.findOne(newDate);
        return keys.get(index);
    }

    public String getStartDateKey(String periodType, java.util.Date startDate, TargetRepositoryDate targetRepositoryDate, String clientId) {
        int index = Util.getIndexOfDateKey(periodType);
        if(index==-1){
            logger.error("Improper period passssed "+index);
        }
        if (startDate == null) {
            logger.info("The start date is not present "+startDate+periodType);
            Date sqlDate = new Date(System.currentTimeMillis());
            List<String> keys = dateKeyCache.getLiveKeys(clientId,sqlDate);
            return keys.get(index);
        } else {
            Date sqlDate = new Date(startDate.getTime());
            List<String> keys = dateKeyCache.getLiveKeys(clientId,sqlDate);
            return keys.get(index);
        }
    }

    public int getNoOfRepetetions(java.util.Date starDate, java.util.Date endDate, String period, String clientCode) throws ExcelRowException{
        if ((starDate==null && starDate.toString().isEmpty()) || (endDate==null && endDate.toString().isEmpty())){
            return 0;
        } else {
            long timediff = endDate.getTime()-starDate.getTime();
            int noOfRepetetions = 0;
            if (timediff<0){
                throw  new ExcelRowException("Stardate cannot be greater than endDate");
            } else {
                Date sqlStartDate = new Date(starDate.getTime());
                Date sqlEndDate = new Date(endDate.getTime());
                List<String> dateKeys = dateKeyCache.getKeysForPeriodAndDateRange(clientCode,sqlStartDate,sqlEndDate,period);
                HashSet<String> hashSet = new HashSet<>(dateKeys);
                logger.info("noOFRepetetions"+hashSet.size());
                return hashSet.size();
                /*long noOfDays = TimeUnit.DAYS.convert(timediff,TimeUnit.MILLISECONDS);
                logger.info("No of days"+noOfDays);

                switch (period){
                    case "week":
                        *//*if (noOfDays<7){
                            throw new ExcelRowException("For week period the startdate and end date must be atleast 7 days");
                        }*//*
                        noOfRepetetions = (int)Math.ceil((double) noOfDays/7);;
                        break;
                    case "day":
                        noOfRepetetions = (int)noOfDays;
                        break;
                    case "quarter":
                        *//*if (noOfDays<90){
                            throw new ExcelRowException("For quarter period the startdate and end date must be atleast 90 days");
                        }*//*
                        noOfRepetetions = (int)Math.ceil((double)noOfDays/90);
                        break;
                    case "month":
                        *//*if (noOfDays<30){
                            throw new ExcelRowException("For month period the startdate and end date must be atleast 30 days");
                        }*//*
                        noOfRepetetions = (int)Math.ceil((double)noOfDays/30);
                        break;
                    case "year":
                        *//*if (noOfDays<365){
                            throw new ExcelRowException("For year period the startdate and end date must be atleast 365 days");
                        }*//*
                        noOfRepetetions = (int)Math.ceil((double)noOfDays/365);
                        break;
                }*/
            }

//            return noOfRepetetions;
        }
    }

}
