package com.getvymo.targets.data.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getvymo.cache.fact_metrics.IFactsMetricsCache;
import com.getvymo.cache.fact_metrics.FactsMetricsCacheImpl;
import com.getvymo.cache.facts.IFactsCache;
import com.getvymo.cache.facts.FactsCacheImpl;
import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abhishek on 26/03/17.
 */
public class MetricFactParser {
    @Autowired
    private FactsMetricsRepository factsMetricsRepository;
    @Autowired
    private FactsRepository factsRepository;

    ObjectMapper mapper = new ObjectMapper();

    ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();

    public String getFactId(int metricFactId){
        IFactsMetricsCache iFactsMetricsCache = new FactsMetricsCacheImpl();
        String factId = iFactsMetricsCache.getFactId(metricFactId);
        return factId;
    }
    public String getFactValue(String metricFactId) {
        IFactsCache iFactsCache = new FactsCacheImpl();
//        Facts factMetric = factsRepository.findOne(metricFactId);
        String factValueString = iFactsCache.getFactValue(metricFactId);
        System.out.println("JSONELEMENT"+metricFactId);
//        String factValueString = factMetric.getState();
        return factValueString;
    }

    /*public JsonObject getFactValue(String metricFactId) {
        IFactsCache iFactsCache = new FactsCacheImpl();
//        Facts factMetric = factsRepository.findOne(metricFactId);
        String factValueString = iFactsCache.getFactValue(metricFactId);
        System.out.println("JSONELEMENT"+metricFactId);
//        String factValueString = factMetric.getState();
        if(factValueString==null || factValueString.isEmpty()){
            return null;
        }
        Gson gson = new Gson();
        try {
            JsonElement jelem = gson.fromJson(factValueString, JsonElement.class);
            System.out.println("JSONELEMENT"+jelem.toString()+jelem);
            JsonObject jobj = jelem.getAsJsonObject();

            return jobj;
        } catch (  JsonSyntaxException e){
            System.out.println("eror while Parsing JSON");
            e.printStackTrace();
            JsonParser jsonParser = new JsonParser();
            JsonReader reader = new JsonReader(new StringReader(factValueString));
            reader.setLenient(true);
            JsonElement jsonObject= jsonParser.parse(reader);
            JsonObject jobj = jsonObject.getAsJsonObject();
            System.out.println("JSONELEMENT"+jsonObject.toString()+jsonObject);
            return jobj;
        }
    }*/

    public String getFactDimensions(int metricFactId) {
        IFactsMetricsCache iFactsMetricsCache = new FactsMetricsCacheImpl();
        ArrayList<FactMetrics> factMetric = iFactsMetricsCache.getFactMetrics(metricFactId);
//        ArrayList<FactMetrics> factMetric = factsMetricsRepository.findById(metricFactId);
        HashMap<String,String> dummyMap = new HashMap<>();
        for (FactMetrics factMetrics: factMetric){
                dummyMap.put(factMetrics.getFactDimension(),factMetrics.getValue());
        }
        Gson gson = new Gson();

        String jobj = gson.toJson(dummyMap);

        return jobj;
    }

    public String createValuString(HashMap map){
        Gson gson = new Gson();
        String valueString = gson.toJson(map);

        return  valueString;
    }
}
