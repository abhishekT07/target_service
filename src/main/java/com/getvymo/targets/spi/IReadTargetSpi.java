package com.getvymo.targets.spi;

import java.sql.Date;
import java.util.List;


import com.getvymo.data.response.entities.ActiveTargets;
import com.getvymo.targets.spi.filters.UserFilter;

import com.getvymo.targets.spi.filters.DateFilter;
import com.getvymo.targets.spi.filters.MetricsFilter;


public interface IReadTargetSpi {


	public List<ActiveTargets> listTargets(UserFilter filter, DateFilter dateFilter);


	

	public  List<ActiveTargets> getTargetDetails(UserFilter filter, MetricsFilter tdFilter, Date startDate, Date endDate);

	public  List<ActiveTargets> getSummary(UserFilter filter, DateFilter dateFilter);

}
