package com.getvymo.targets.spi;

/**
 * Created by abhishek on 21/04/17.
 */
public interface ITargetMgmntSpi {

    public String getClientTargetInstanceList(String clientCode);
}
