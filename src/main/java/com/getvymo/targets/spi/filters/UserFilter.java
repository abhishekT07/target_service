package com.getvymo.targets.spi.filters;



import org.springframework.beans.factory.annotation.*;

//@Configurable
public class UserFilter {
	

	private String clientId;
	private String self;
	
	private String userId;
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getSelf() {
		return self;
	}
	public void setSelf(String self) {
		this.self = self;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
