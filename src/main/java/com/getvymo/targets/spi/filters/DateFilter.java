package com.getvymo.targets.spi.filters;

import java.sql.Date;
import org.springframework.beans.factory.annotation.*;

//@Configurable
public class DateFilter {
	private Date startDate;
	private Date endDate;
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
