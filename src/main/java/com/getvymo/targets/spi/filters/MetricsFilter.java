package com.getvymo.targets.spi.filters;
import org.springframework.beans.factory.annotation.*;

public class MetricsFilter {
	
	private String targetDefintionId;
	private String metricFactId;
	private String achievementType;
	public String getTargetDefintionId() {
		return targetDefintionId;
	}
	public void setTargetDefintionId(String targetDefintionId) {
		this.targetDefintionId = targetDefintionId;
	}
	public String getMetricFactId() {
		return metricFactId;
	}
	public void setMetricFactId(String metricFactId) {
		this.metricFactId = metricFactId;
	}
	public String getAchievementType() {
		return achievementType;
	}
	public void setAchievementType(String achievementType) {
		this.achievementType = achievementType;
	}

}
