package com.getvymo.targets.spi;

public interface AbstractTargetFactory {
	
	public IReadTargetSpi getTargets();

}
