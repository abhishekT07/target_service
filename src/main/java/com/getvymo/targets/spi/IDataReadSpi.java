package com.getvymo.targets.spi;

import java.sql.Date;
import java.util.List;

import com.getvymo.data.response.entities.ActiveTargets;

public interface IDataReadSpi {
	
	
	public List<ActiveTargets> listActiveTargetsForUser(String userId, String clientId, String self);
	public List<ActiveTargets> listTargetsByDate(String userId, String self, String clientId, Date startDate,
												 Date endDate);
	public List<ActiveTargets> getTargetDetailsByUser(String userId, String clientId, String targetDefinitionId,
													  String metricFactId, String achievementType, Date startDate, Date endDate);
	public List<ActiveTargets> getTargetsSummary(String userId, String self, String clientId, Date startDate,
												 Date endDate);


}
