package com.getvymo.targets.spi.impl;

import com.getvymo.data.api.DeleteTargetImpl;
import com.getvymo.data.api.IDeletetarget;
import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.exceptions.ErrorException;
import com.getvymo.targets.spi.IDeleteTargetSpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by abhishek on 20/04/17.
 */
@Component
public class DeleteTargetSpiImpl implements IDeleteTargetSpi {
    @Autowired
    IDeletetarget deleteTarget;

    @Override
    public boolean deleteTargetForClient(int instanceId, String clientCode) throws ErrorException {
        TargetInstance targetInstance = deleteTarget.isClientTarget(instanceId, clientCode);

        if (targetInstance!=null){
            //proceed with deletion
            return deleteTarget.deleteTarget(targetInstance);
        }
        return true;
    }
}
