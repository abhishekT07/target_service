package com.getvymo.targets.spi.impl;

import com.getvymo.cache.IDateKeyCache;
import com.getvymo.data.api.ITargetMgmnt;
import com.getvymo.data.response.entities.TargetDescription;
import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.targets.spi.ITargetMgmntSpi;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 21/04/17.
 */
@Component
public class TargetMgmntSpiImpl implements ITargetMgmntSpi{
    Logger logger = LoggerFactory.getLogger(TargetMgmntSpiImpl.class);
    @Autowired
    ITargetMgmnt iTargetMgmnt;
    @Autowired
    IDateKeyCache iDateKeyCache;
    @Override
    public String getClientTargetInstanceList(String clientCode) {
        try {
            List<TargetInstance> targetInstanceList = iTargetMgmnt.fetchClientTargets(clientCode);
            HashMap<Integer,String> metricFactDescriptions = iTargetMgmnt.fetchClientMetricFacts(clientCode);
            HashMap<Integer,TargetDescription> returnTargetsMap = new HashMap<Integer,TargetDescription>();

            for (TargetInstance targetInstance: targetInstanceList){
                List<Date> dateList = iDateKeyCache.getStartAndEndDate(clientCode,targetInstance.getDateKey());
                String dimensionString = metricFactDescriptions.get(targetInstance.getMetricFactId());
                returnTargetsMap.put(targetInstance.getId(), new TargetDescription(targetInstance.getLabel(),
                        dimensionString, dateList.get(0).getTime(), dateList.get(1).getTime() + 86399999));
            }
            String jsonString = new Gson().toJson(returnTargetsMap,returnTargetsMap.getClass());
            return jsonString;
        } catch (Exception e){
            logger.error("Error Occured while fecthing target instance list"+e.getMessage());
            e.printStackTrace();
            return "Could not fetch Target metadata, please try again later";
        }
    }
}
