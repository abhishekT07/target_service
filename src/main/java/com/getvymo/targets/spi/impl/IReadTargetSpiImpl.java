package com.getvymo.targets.spi.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.getvymo.targets.spi.IDataReadSpi;
import com.getvymo.targets.spi.IReadTargetSpi;
import com.getvymo.targets.spi.filters.DateFilter;
import com.getvymo.targets.spi.filters.UserFilter;
import com.getvymo.targets.spi.filters.MetricsFilter;

import org.slf4j.LoggerFactory;
import com.getvymo.data.response.entities.ActiveTargets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

@Component
public class IReadTargetSpiImpl implements IReadTargetSpi {
	
	@Autowired
	IDataReadSpi dataRead;

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(IReadTargetSpiImpl.class);

	@Override

	public List<ActiveTargets> listTargets(UserFilter userfilter, DateFilter dateFilter) {

		// TODO Auto-generated method stub

		List<ActiveTargets> activeTargets = new ArrayList<ActiveTargets>();
		//DataRead data = new DataRead();

		if (dateFilter.getStartDate() != null && dateFilter.getEndDate() != null) {
			logger.info("list all the targets by date ");
			List<ActiveTargets> listActiveTargetsForHierarchyByDate = dataRead.listTargetsByDate(
					userfilter.getUserId(), userfilter.getClientId(), userfilter.getSelf(), dateFilter.getStartDate(),
					dateFilter.getEndDate());
			return listActiveTargetsForHierarchyByDate;

		} else {
			
			
			logger.info("inside list all active targets");

			activeTargets = dataRead.listActiveTargetsForUser(userfilter.getUserId(), userfilter.getClientId(),userfilter.getSelf());
			return activeTargets;
		}

	}

	@Override
	public List<ActiveTargets> getTargetDetails(UserFilter filter, MetricsFilter tdFilter, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		logger.info("deatiled targets");

		List<ActiveTargets> activeTargets = new ArrayList<ActiveTargets>();
	
		activeTargets = dataRead.getTargetDetailsByUser(filter.getUserId(), filter.getClientId(),
				tdFilter.getTargetDefintionId(), tdFilter.getMetricFactId(), tdFilter.getAchievementType(), startDate, endDate);
		return activeTargets;
	}

	@Override
	public  List<ActiveTargets> getSummary(UserFilter filter, DateFilter dateFilter){
		List<ActiveTargets> activeTargets = new ArrayList<ActiveTargets>();
		activeTargets = dataRead.getTargetsSummary(filter.getUserId(), filter.getSelf(),filter.getClientId(),dateFilter.getStartDate(),dateFilter.getEndDate());
		return activeTargets;
	}

}
