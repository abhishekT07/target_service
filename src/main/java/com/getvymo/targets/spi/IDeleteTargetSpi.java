package com.getvymo.targets.spi;

import com.getvymo.exceptions.ErrorException;

/**
 * Created by abhishek on 20/04/17.
 */
public interface IDeleteTargetSpi {
    public boolean deleteTargetForClient(int instanceId, String clientCode) throws ErrorException;
}
