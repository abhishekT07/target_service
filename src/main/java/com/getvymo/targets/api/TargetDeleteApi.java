package com.getvymo.targets.api;

import com.getvymo.exceptions.ErrorException;
import com.getvymo.targets.spi.impl.DeleteTargetSpiImpl;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created by abhishek on 20/04/17.
 */
@RestController
@RequestMapping(value = "/targets/v1/target")
public class TargetDeleteApi {
    Logger logger = LoggerFactory.getLogger(TargetDeleteApi.class);
    @Autowired
    DeleteTargetSpiImpl deleteTargetSpi;

    @RequestMapping(value = "/{instanceId}", params = {"client_code"}, method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteTarget(@PathVariable("instanceId") int instanceId, @RequestParam("client_code") String
            clientCode) {
        HashMap<String, String> responseMap = new HashMap<>();
        try {
            boolean status = deleteTargetSpi.deleteTargetForClient(instanceId, clientCode);

        } catch (ErrorException e) {
            logger.error("Target deletion failed for " + e.getMessage());
            responseMap.put("error", "Failed to delete");
            e.printStackTrace();
            return new ResponseEntity<String>(new Gson().toJson(responseMap, responseMap.getClass()), HttpStatus.OK);
        }
        responseMap.put("status", "Successfully deleted");
        return new ResponseEntity<String>(new Gson().toJson(responseMap, responseMap.getClass()), HttpStatus.OK);
    }
}
