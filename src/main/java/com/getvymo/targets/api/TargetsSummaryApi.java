package com.getvymo.targets.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getvymo.data.response.entities.ActiveTargets;
import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetRepositoryDate;
import com.getvymo.data.sql.repositoryHelper.CustomResponse;
import com.getvymo.targets.spi.AbstractTargetFactory;
import com.getvymo.targets.spi.IReadTargetSpi;
import com.getvymo.targets.spi.filters.DateFilter;
import com.getvymo.targets.spi.filters.MetricsFilter;
import com.getvymo.targets.spi.filters.UserFilter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.util.List;

//import com.getvymo.data.response.entities.ManagerHierarchy;
//import com.getvymo.services.ActiveTargetsRepository;

@RestController
@RequestMapping("/targets/v1/user")
public class TargetsSummaryApi {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private FactsMetricsRepository factsMetricsRepository;
	@Autowired
	private FactsRepository factsRepository;

	private AbstractTargetFactory targetFcatory;
	@Autowired
	private TargetRepositoryDate targetRepositoryDate;
	
	@Autowired
	IReadTargetSpi targets;

	
	//mapper.setSerializationInclusion(Include.NON_NULL);
	
	
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(TargetsSummaryApi.class);

	// API for get all the active targets for agents
	@RequestMapping(value = "/{userid}/summary", params = { "clientcode", "self"}, method = RequestMethod.GET, produces = "application/json")
	public String listAllTargets(@PathVariable(value = "userid") String userId,
			@RequestParam("clientcode") String clientId, @RequestParam("self") String self,@RequestParam(value = "start", required = false) Date startDate,
								 @RequestParam(value = "end", required = false) Date endDate) {
		ObjectMapper mapper = new ObjectMapper();
		 //mapper.setSerializationInclusion(Include.NON_NULL);
		ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
		String jsonString = null;
		UserFilter userfilter = new UserFilter();
		DateFilter dateFilter = new DateFilter();
		MetricsFilter tdFilter = new MetricsFilter();
		userfilter.setClientId(clientId);
		userfilter.setSelf(self);
		userfilter.setUserId(userId);

		dateFilter.setStartDate(startDate);
		dateFilter.setEndDate(endDate);

		/*targetFcatory = new ActiveTargetsFactory();
		IReadTargetSpi targets = targetFcatory.getTargets();*/
		List<ActiveTargets> activeTargets = targets.getSummary(userfilter,dateFilter);

		try {
			CustomResponse<ActiveTargets> customResponse = new CustomResponse(activeTargets);
			jsonString = writer.writeValueAsString(customResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString;
	}

}
