package com.getvymo.targets.api;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by abhishek on 19/04/17.
 */
@RestController
@RequestMapping(value = "/targets")
public class Health {

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public ResponseEntity<String> health(){
        return new ResponseEntity<String>(new Gson().fromJson("{\"success\":1}", JsonObject.class).toString(), HttpStatus.OK);
    }
}
