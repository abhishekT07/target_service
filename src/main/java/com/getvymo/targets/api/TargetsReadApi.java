package com.getvymo.targets.api;

import java.sql.Date;

import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetRepositoryDate;

import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.getvymo.data.response.entities.ActiveTargets;
//import com.getvymo.data.response.entities.ManagerHierarchy;
import com.getvymo.data.sql.repositoryHelper.CustomResponse;

import com.getvymo.targets.spi.AbstractTargetFactory;
import com.getvymo.targets.spi.IReadTargetSpi;
import com.getvymo.targets.spi.filters.DateFilter;
import com.getvymo.targets.spi.filters.UserFilter;
import com.getvymo.targets.spi.filters.MetricsFilter;

//import com.getvymo.services.ActiveTargetsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.*;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/targets/v1/user")
public class TargetsReadApi {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private FactsMetricsRepository factsMetricsRepository;
	@Autowired
	private FactsRepository factsRepository;

	private AbstractTargetFactory targetFcatory;
	@Autowired
	private TargetRepositoryDate targetRepositoryDate;
	
	@Autowired
	IReadTargetSpi targets;

	
	//mapper.setSerializationInclusion(Include.NON_NULL);
	
	
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(TargetsReadApi.class);

	// API for get all the active targets for agents
	@RequestMapping(value = "/{userid}", params = { "clientcode", "self"}, method = RequestMethod.GET, produces = "application/json")
	public String listAllTargets(@PathVariable(value = "userid") String userId,
			@RequestParam("clientcode") String clientId, @RequestParam("self") String self,
			@RequestParam(value = "start", required = false) Date startDate,
			@RequestParam(value = "end", required = false) Date endDate,
			@RequestParam(value = "targetdefinitionid", required = false) String targetDefinitionId,
			@RequestParam(value = "metricfactid", required = false) String metricFactId,
								 @RequestParam(value = "achievementType", required = false) String achievementType) {
		ObjectMapper mapper = new ObjectMapper();
		 //mapper.setSerializationInclusion(Include.NON_NULL);
		ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
		String jsonString = null;
		UserFilter userfilter = new UserFilter();
		DateFilter dateFilter = new DateFilter();
		MetricsFilter tdFilter = new MetricsFilter();
		userfilter.setClientId(clientId);
		userfilter.setSelf(self);
		userfilter.setUserId(userId);
		dateFilter.setStartDate(startDate);
		dateFilter.setEndDate(endDate);
		tdFilter.setTargetDefintionId(targetDefinitionId);
		tdFilter.setMetricFactId(metricFactId);
		tdFilter.setAchievementType(achievementType);

		/*targetFcatory = new ActiveTargetsFactory();
		IReadTargetSpi targets = targetFcatory.getTargets();*/

		if(targetDefinitionId==null&&metricFactId==null)
		{
		List<ActiveTargets> activeTargets = targets.listTargets(userfilter, dateFilter);
		logger.info("result of active targets" + activeTargets);
		try {
			CustomResponse<ActiveTargets> customResponse = new CustomResponse(activeTargets);
			jsonString = writer.writeValueAsString(customResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonString;
		}
		else
		{
			List<ActiveTargets> activeTargets = targets.getTargetDetails(userfilter, tdFilter,startDate,endDate);
			logger.info("result of active targets" + activeTargets);
			try {
				CustomResponse<ActiveTargets> customResponse = new CustomResponse(activeTargets);
				jsonString = writer.writeValueAsString(customResponse);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonString;
		}
	}

	// Manager email summary detailed view

	// @RequestMapping(value = "/targets/v1/target", params = {
	// "clientid" }, method = RequestMethod.GET, produces = "application/json")
	// public String listUserandManagerTargets(@RequestParam("clientid") String
	// clientId) {
	// String jsonString = null;
	//
	//
	// List<LevelwiseSummary> result = new
	// DataRead().findAllHierarchyInstancesGroupByUser(clientId, entityManager);
	// try {
	// CustomResponse<Users> customResponse = new CustomResponse(result);
	// jsonString = writer.writeValueAsString(customResponse);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	//
	// return jsonString;
	// }

}
