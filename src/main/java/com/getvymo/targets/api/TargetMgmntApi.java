package com.getvymo.targets.api;

import com.getvymo.targets.spi.ITargetMgmntSpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by abhishek on 20/04/17.
 */
@RestController
@RequestMapping(value = "/targets/v1/target")
public class TargetMgmntApi {

    @Autowired
    ITargetMgmntSpi iTargetMgmntSpi;
    @RequestMapping(value = "/{clientCode}", method = RequestMethod.GET)
    public ResponseEntity<String> fetchListOfClientTargets(@PathVariable("clientCode") String clientCode){


        return new ResponseEntity<String>(iTargetMgmntSpi.getClientTargetInstanceList(clientCode), HttpStatus.OK);
    }
}
