package com.getvymo.service;

import com.getvymo.batch_job.TargetScheduler;
import com.getvymo.data.api.PersistTarget;
import com.getvymo.data.api.PersistTargetImpl;
import com.getvymo.data.sql.entities.target_entities.*;
import com.getvymo.targets.data.api.DateKeysHelper;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsRolesRepository;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UsersRepository;
import com.getvymo.data.sql.repositories.target_repos.*;
import com.getvymo.importer.Importer;
import com.getvymo.importer.RowStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by abhishek on 17/03/17.
 */
@Controller
public class CreateTarget {
    private static Logger logger = LoggerFactory.getLogger(CreateTarget.class);
    @Autowired
    private static TargetInstanceRepository targetInstanceRepository;
    @Autowired
    private static UserVsTargetInstanceRepository userVsTargetInstanceRepository;
    @Autowired
    private static TargetStatusRepository targetStatusRepository;
    @Autowired
    private static TargetRepositoryDate repositoryDate;
    @Autowired
    private FactsRepository factsRepository;
    @Autowired
    private DateKeysHelper dateKeysHelper;


    public Facts random() {
        return factsRepository.findOne("sbi_banca_activity_count");
    }

    public HashMap<Integer,RowStatus> createBulkTargets(HashMap<Integer,ArrayList<HashMap<String,String>>> integerArrayListHashMap,
   String clientCode, int noOfRows, TargetInstanceRepository targetInstanceRepository, UserVsTargetInstanceRepository
   userVsTargetInstanceRepository, TargetStatusRepository targetStatusRepository, TargetRepositoryDate targetRepositoryDate){

        logger.info("Staring target creation for bulk upload for rows"+noOfRows+integerArrayListHashMap.size());
        HashMap<Integer,RowStatus> statusHashMap = new HashMap<Integer,RowStatus>();
        for (int i=0; i<noOfRows;i++){
            ArrayList<HashMap<String,String>>  currHashMapArrayList = integerArrayListHashMap.get(i);
            if (currHashMapArrayList!=null){
                logger.info("Non empty target info found for row no"+i);
                RowStatus currentRowStatus = createTargets(currHashMapArrayList.get(0), currHashMapArrayList.get(1),
                        clientCode, targetInstanceRepository, userVsTargetInstanceRepository, targetStatusRepository,
                        targetRepositoryDate);
                statusHashMap.put(i,currentRowStatus);
            } else {
                logger.info("Empty target info found for row no"+i);
                statusHashMap.put(i,new RowStatus(true,null));
            }

        }
        return statusHashMap;
    }

    /**
     * Accepts metadata like fact_id, metric_fact_id, def_id
     * accepts info like repeating? how many times, userCode, role(if any) achievement criteria, assignment criteria
     *  and target value
     * @param metaData
     * @param info
     * @return
     */
    public RowStatus createTargets(HashMap<String, String> metaData, HashMap<String, String> info, String clientCode,
              TargetInstanceRepository targetInstanceRepository, UserVsTargetInstanceRepository
              userVsTargetInstanceRepository, TargetStatusRepository targetStatusRepository, TargetRepositoryDate
                                                  targetRepositoryDate) {
        RowStatus currentRowStatus =null;
        PersistTarget persistTarget = new PersistTargetImpl();
        int noOfDuplicateTargets = 0;
        // first check if this target is of recurring types
        if (metaData.containsKey("repeating") && metaData.get("repeating").equals("yes")){
            logger.info("Repeating targets found"+metaData.get("noOfRepetetions"));
            int noOfRepetetions = Integer.parseInt(metaData.get("noOfRepetetions"));
            if (noOfRepetetions>0){
                while (noOfDuplicateTargets <= noOfRepetetions) {

                    currentRowStatus = persistTarget.createIndividualInstances(metaData,info,clientCode,
                            targetInstanceRepository,userVsTargetInstanceRepository,targetStatusRepository);

                    if (!currentRowStatus.getSuccessful()){
                        return currentRowStatus;
                    }
                    //handle date keys logic here itself
                    String newDateKey = dateKeysHelper.getNextDateKey(noOfDuplicateTargets,info.get("period"),
                            targetRepositoryDate, metaData.get("startDate"),clientCode);
                    metaData.put("date_key",newDateKey);
                    noOfDuplicateTargets++;
                }
                return currentRowStatus;
            }

        } else {
            currentRowStatus = persistTarget.createIndividualInstances(metaData, info, clientCode,
                    targetInstanceRepository, userVsTargetInstanceRepository, targetStatusRepository);
            return currentRowStatus;
            /*System.out.println("Returned instance"+returned.getClientCode());
            returnInstanceList.add(returned);*/
        }
        return new RowStatus(false,"Failed");
    }

}
