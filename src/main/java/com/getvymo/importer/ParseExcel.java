package com.getvymo.importer;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by abhishek on 17/03/17.
 */
public class ParseExcel {
    private static Logger logger = LoggerFactory.getLogger(ParseExcel.class);
    /*public static String[] GetDataFromExcel(MultipartFile file){
        if(!file.isEmpty()){
            System.out.println("@least it is not empty");
            try {
                byte[] bytes = file.getBytes();
                String completeData = new String(bytes);
                String[] rows = completeData.split("\n");
                String[] columns = rows[0].split(",");
                System.out.println(rows[0]+"dasaa"+ columns[1]+" "+columns[2]+ columns.toString());
            } catch (Exception e){

            }
        }
        return null;
    }*/
    public static Sheet createTempFile(MultipartFile file, String clientCode){
        File convFile = new File( clientCode+"-"+System.currentTimeMillis());
        Sheet sheet=null;
        try {
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
            FileInputStream inp = new FileInputStream(convFile);
            Workbook workbook = WorkbookFactory.create(inp);
            sheet = workbook.getSheetAt( 0 );
        } catch (IOException | InvalidFormatException e){
            System.out.print("Exception occured");
            e.printStackTrace();
        }
        return sheet;

    }

    public static ArrayList<JSONArray> GetDataFromExcel(Sheet sheet) throws Exception{

        logger.info("Extracting data from excel",sheet.getSheetName());
        JSONObject json = new JSONObject();
        ArrayList<JSONArray> jsonArrays = new ArrayList<>();
        try {

            System.out.println("SheetName"+sheet.getSheetName());
            // Iterate through the rows.
            JSONArray rows = new JSONArray();
            for (Iterator<Row> rowsIT = sheet.rowIterator(); rowsIT.hasNext(); )
            {
                Row row = rowsIT.next();
                JSONObject jRow = new JSONObject();

                // Iterate through the cells.
                JSONArray cells = new JSONArray();
                for (Iterator<Cell> cellsIT = row.cellIterator(); cellsIT.hasNext(); )
                {
                    Cell cell = cellsIT.next();
                    switch (cell.getCellType()){
                        case 0:
                            if (HSSFDateUtil.isCellDateFormatted(cell)){
                                cells.add(cell.getDateCellValue());
                                Date date = new Date(cell.getDateCellValue().getTime());
                                System.out.println("Got date value"+cell.getDateCellValue()+cells.toString()+date);
                            } else {
                                cells.add(cell.getNumericCellValue());
                            }
                            break;
                        case 1:
                            cells.add( cell.getStringCellValue());
                            break;
                        default:
                            cells.add( cell.getStringCellValue() );
                    }

                }
                jRow.put( "cell", cells );
                jsonArrays.add(cells);
                rows.add( jRow );
            }
            json.put( "rows", rows );
        } catch (Exception e){
            logger.error("Problem while getting data from excel");
            e.printStackTrace();
            throw new Exception("Problem while getting data from excel");
        }
        return jsonArrays;
    }

    public static String[] getCsvHeader(String[] rows){
        String[] headers = rows[0].split(",");
        return headers;
    }

}
