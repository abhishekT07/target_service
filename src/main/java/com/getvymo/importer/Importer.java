package com.getvymo.importer;

/**
 * Created by abhishek on 16/03/17.
 */

import com.getvymo.batch_job.TargetScheduler;
import com.getvymo.data.api.ITargetMgmnt;
import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsRoles;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsRolesRepository;
import com.getvymo.exceptions.ExcelRowException;
import com.getvymo.service.CreateTarget;
import com.getvymo.data.sql.entities.target_entities.*;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsAccessiblehierarchyKeysRepository;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UsersRepository;
import com.getvymo.data.sql.repositories.target_repos.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.simple.JSONArray;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;


@RestController
@RequestMapping("/targets/v1/import")
public class Importer {

    private static Logger logger = LoggerFactory.getLogger(Importer.class);
    @Autowired
    private TargetRespository repo;
    @Autowired
    private UserVsAccessiblehierarchyKeysRepository userVsAccessiblehierarchyKeysRepository;
    @Autowired
    private FactsRepository factsRepository;
    @Autowired
    private FactsMetricsRepository factsMetricsRepository;
    @Autowired
    private TargetRepositoryDate targetRepositoryDate;
    @Autowired
    private TargetDefinitionRepository targetDefinitionRepository;
    @Autowired
    private TargetInstanceRepository targetInstanceRepository;
    @Autowired
    private UserVsTargetInstanceRepository userVsTargetInstanceRepository;
    @Autowired
    private TargetStatusRepository targetStatusRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private DimensionsRepository dimensionsRepository;
    @Autowired
    private UserVsRolesRepository userVsRolesRepository;
    @Autowired
    private CreateTarget createTarget;
    @Autowired
    private ImportHelper importHelper;
    @Autowired
    private ITargetMgmnt iTargetMgmnt;

    @RequestMapping(value = "/client/{clientCode}", method = RequestMethod.GET)
    public @ResponseBody String returnConfigurableTargets(@PathVariable("clientCode") String clientCode){
        //ImportHelper importHelper= new ImportHelper();
        HashMap<Integer,String> clientMap = iTargetMgmnt.fetchClientMetricFacts(clientCode);
        String jsonObject = new Gson().toJson(clientMap);
        return jsonObject;
    }

    @RequestMapping( value = "/{clientCode}", method = RequestMethod.POST)
    public ResponseEntity<Resource> ImportTarget(@RequestPart("file") MultipartFile file,
                                                 @PathVariable("clientCode") String clientCode, @RequestPart("params") String params){
        String fileName = file.getOriginalFilename();
        logger.info("Received Bulk import file",fileName);
        //ImportHelper importHelper = new ImportHelper();
        HashMap<String,String> targetMetadata=null;
        try{
             targetMetadata = importHelper.validateAndGetTargetMetadata( params, clientCode);
        } catch (ExcelRowException e){
            ArrayList<String> newL =  new ArrayList<String>();
            logger.error("Error while processing metadata, returning response with error message");
            newL.add(0,e.getMessage());
            return importHelper.sendCSV(importHelper.buildResponseData(e.getMessage()),"errorStatus");
        }

        //Returning error response as this target cannot be created because dimension combination not supported
        if(targetMetadata.isEmpty()){
            /*ArrayList<String> newL =  new ArrayList<String>();
            newL.add(0,"This target cannot be created as of now, please contact Vymo support");*/
            return importHelper.sendCSV(importHelper.buildResponseData("This target cannot be created as of now, please contact Vymo support"),"errorStatus");
        }

        //Check if this fact id and metric fact id are plausible for this client Get the mfID from request
        Sheet sheet = ParseExcel.createTempFile(file,clientCode);
        ArrayList<JSONArray> data=null;
        try {
            data = ParseExcel.GetDataFromExcel(sheet);

        } catch (Exception e){
            /*ArrayList<String> newL =  new ArrayList<String>();
            newL.add(0,e.getMessage());*/
            return importHelper.sendCSV(importHelper.buildResponseData(e.getMessage()),"errorStatus");
        }

        JSONArray header = data.get(0);
        //ParseExcel.getCsvHeader(data);

        //Check if headers are not tampered with
        if(!importHelper.validateHeaders(header)){
            /*ArrayList<String> newL =  new ArrayList<String>();
            newL.add(0,"The excel sheet headers have been altered, please download the provided excel again and try again :)");*/
            return importHelper.sendCSV(importHelper.buildResponseData("The excel sheet headers have been altered, please download the provided excel again and try again :)"),"errorStatus");
        }
        JSONArray responseHeader = header;
        String sheetName = sheet.getSheetName();
        //Adding status column to header array
        responseHeader.add(header.size(),"Status");


        logger.info("Starting the Target processing now");
        Gson gson = new Gson();
//        Facts clientFact = importHelper.validateTargetMetadata(sheetName,header,clientCode,factsRepository);
//        HashMap<Integer,HashMap<Boolean,String>>
        HashMap<Integer,ArrayList<HashMap<String,String>>> rowWiseTargetData = new
                HashMap<Integer,ArrayList<HashMap<String,String>>>();
        if (!targetMetadata.isEmpty()) {
            logger.info("Found client metadata now processing %d rows", data.size());
            for (int i = 1; i < data.size(); i++) {
                JSONArray row = data.get(i);
                //Validate against facts and Dimensions
                logger.info("Processing row no."+(i+1));
                try {
                    HashMap<String, String> metadata = importHelper.getTargetMetaData(sheetName, data.get(i), clientCode, header,
                            (HashMap<String, String>) targetMetadata.clone(), targetDefinitionRepository, targetRepositoryDate);
                    if (metadata == null) {
                        continue;
                    }
                    logger.info("Received target metadata, now fetching the target info");
                    HashMap<String, String> info = importHelper.getTargetInstanceInfo(sheetName, data.get(i), header,
                            clientCode);

                    logger.info("Received target info, now creating the targetImpl info");
                    ArrayList<HashMap<String,String>> metaInfoList = new ArrayList<HashMap<String, String>>();
                    metaInfoList.add(0,metadata);
                    metaInfoList.add(1,info);
                    rowWiseTargetData.put(i,metaInfoList);

                } catch (ExcelRowException e) {
                    rowWiseTargetData.put(i,null);
                    logger.info("Exception occurred in row no"+i+e.getMessage());
                    row.add(row.size(), e.getMessage());
                    e.printStackTrace();
                }
            }

            //Now Parsing is done  create targets
            HashMap<Integer, RowStatus> rowStatus = createTarget.createBulkTargets(rowWiseTargetData, clientCode,
                    data.size(), targetInstanceRepository, userVsTargetInstanceRepository, targetStatusRepository,
                    targetRepositoryDate);
            //Now update the row wise status
            for (int i=0; i<data.size();i++){
                RowStatus current = rowStatus.get(i);
                if ( current.getMessage()!=null){
                    JSONArray row = data.get(i);
                    row.add(row.size(), current.getMessage());
                }

            }
            //Finally call scheduler explicitly once the file is processed(Commenting out as it takes a lot of time for organizational level targets)
            /*TargetScheduler.processTargets(targetStatusRepository, targetInstanceRepository, repo,
                    usersRepository, userVsTargetInstanceRepository, userVsRolesRepository);*/

        } else {
            //NEED TO RETURN RESPONSE SAYING INVALID TARGET TYPE
            return importHelper.sendCSV(importHelper.buildResponseData("Invalid target type detected"),"errorStatus");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "multipart/form-data");
        ResponseEntity<MultipartFile> mp = new ResponseEntity<MultipartFile>(file, headers, HttpStatus.OK);
//        return data;
        return importHelper.sendCSV(data,fileName);
    }
}

