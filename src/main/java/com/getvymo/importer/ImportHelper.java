package com.getvymo.importer;

import com.getvymo.cache.fact_metrics.FactsMetricsCacheImpl;
import com.getvymo.cache.fact_metrics.IFactsMetricsCache;
import com.getvymo.cache.facts.FactsCacheImpl;
import com.getvymo.cache.facts.IFactsCache;
import com.getvymo.data.api.PersistTargetDefinition;
import com.getvymo.data.api.PersistTargetDefinitionImpl;
import com.getvymo.data.sql.entities.target_entities.*;
import com.getvymo.targets.data.api.DateKeysHelper;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsAccessiblehierarchyKeysRepository;
import com.getvymo.data.sql.repositories.target_repos.*;
import com.getvymo.exceptions.ExcelRowException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.ws.Response;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by abhishek on 17/03/17.
 */

@Service
public class ImportHelper {
    private static Logger logger = LoggerFactory.getLogger(ImportHelper.class);
    @Autowired
    private FactsRepository factsRepository1;
    @Autowired
    private FactsMetricsRepository factsMetricsRepository;
    @Autowired
    private TargetDefinitionRepository targetDefinitionRepository;
    @Autowired
    private UserVsAccessiblehierarchyKeysRepository userVsAccessiblehierarchyKeysRepository;
    @Autowired
    private DateKeysHelper dateKeysHelper;

    IFactsMetricsCache iFactsMetricsCache = new FactsMetricsCacheImpl();
    IFactsCache iFactsCache = new FactsCacheImpl();
    //    @RequestMapping( value = "/random/why", method = RequestMethod.GET)
//    public Facts random(FactsRepository factsRepository) {
//        return factsRepository.findOne("sbi_banca_activity_count");
//    }
    public List<Facts> getClientFacts(String clientCode, FactsRepository factsRepository){
        return factsRepository.findByClientCode(clientCode);
    }

    public List<Dimensions> getClientDimensions(String clientCode, DimensionsRepository dimensionsRepository){
        return dimensionsRepository.findByClientCode(clientCode);
    }

    /*public HashMap<Integer,String> getClientTargetMap(String clientCode){
        ArrayList<FactMetrics> factMetricss = iFactsMetricsCache.getAllMetricFactsByClient(clientCode);
        HashMap<Integer,String> targetMapping = new HashMap<>();
        for (FactMetrics factMetrics: factMetricss){
            String factMetricDesc = factMetrics.getDesc();
            String factDesc = iFactsCache.getTargetFactDesc(factMetrics.getFactId());
            String finalDesc = (factMetricDesc != null && factMetricDesc.length() > 0) ? factDesc + " for " + factMetricDesc : factDesc;
            targetMapping.put(factMetrics.getId(),finalDesc);
        }
        return targetMapping;
    }*/

    public boolean validateHeaders(JSONArray receivedHeader){
        ArrayList<String> headersConstant = new ArrayList(TargetImportConstants.getHeadersMap().keySet());
        ArrayList<String> arrayList = new ArrayList(receivedHeader.size());
        logger.info("Validating the excel headers");
        for(int i=0;i < receivedHeader.size();i++){
            arrayList.add((String) receivedHeader.get(i));
        }
        if (arrayList.size()!=headersConstant.size()){
            logger.error("Header size is not what expected",arrayList.size(),headersConstant.size());
            return false;
        } else {
            for (String headerElement: headersConstant){
                if (!arrayList.contains(headerElement)){
                    logger.error("Headers are not matching or altered with",arrayList.toString(),headersConstant.size());
                    return false;
                }
            }
        }
        return true;
    }

    public Facts validateTargetMetadata(String fileName, ArrayList<JSONArray> headers, String clientCode,
                                        FactsRepository factsRepository) {
        String fact_id = clientCode.toLowerCase() + "_" + fileName.toLowerCase();
        try {

            System.out.print("Fact id " + fact_id);
            Facts clientFacts = factsRepository.findOne(fact_id);//new Importer().findFacts(fact_id);
            System.out.print("Client fact returned" + clientFacts + (clientFacts == null));
            if (!(clientFacts == null)) {
                return clientFacts;
            }
        } catch (NullPointerException e) {
            //Means there is no fact for such for this client
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Return the target metadata  fact_id, metric_fact_id, def_id
     *
     * @param
     * @return
     */
    public HashMap<String, String> getTargetMetaData (String fileName, JSONArray row, String clientCode, JSONArray header,
                                                     HashMap<String, String> targetMetadata,
                                                     TargetDefinitionRepository targetDefinitionRepository, TargetRepositoryDate targetRepositoryDate) throws ExcelRowException {
        JsonObject json = new JsonObject();
        HashMap<String,String> timePeriodMap = TargetImportConstants.getTimePeriodMap();
        PersistTargetDefinition createTargetDefinition = new PersistTargetDefinitionImpl();
        //Getting the granularity call a function
        int periodIndex = header.indexOf("Period");
        int startDateIndex = header.indexOf("StartDate");
        int endDateIndex = header.indexOf("EndDate");

        //This check needs to be enhanced
        if (periodIndex>=row.size() || startDateIndex>=row.size() || endDateIndex>=row.size()){
            logger.info("header size is not equal to row size"+header.toString()+row.toString());
            throw new ExcelRowException("Column missing in this row");
        }
        logger.info("Period and startDate index are"+periodIndex+" & "+startDateIndex);
        String granularity = row.get(periodIndex).toString();
        if (! timePeriodMap.containsKey(granularity)){
            throw  new ExcelRowException("Invalid time period selected"+granularity);
        }
        String periodType = timePeriodMap.get(granularity);
        logger.info("Period is "+periodType);
        java.util.Date startDate=null;
        String startDateString = row.get(startDateIndex).toString();
        String endDateString = row.get(endDateIndex).toString();
        String dateKey =null;
        Date endDate = null;
        //DateKeysHelper dateKeysHelper = new DateKeysHelper();
        try {
            if (startDateString==null || startDateString.isEmpty()){
                if (!(endDateString==null || endDateString.isEmpty())){
                    throw  new ExcelRowException("StartDate cannot be empty when endDate is present");
                }

                dateKey = dateKeysHelper.getStartDateKey(periodType, null, targetRepositoryDate, clientCode);
                targetMetadata.put("startDate",null);
            } else {
                DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                logger.info("Stardate"+row.get(startDateIndex).toString());
                startDate = dateFormat.parse(row.get(startDateIndex).toString());
                dateKey = dateKeysHelper.getStartDateKey(periodType, startDate, targetRepositoryDate, clientCode);
                targetMetadata.put("startDate",startDate.toString());
            }
            if (startDate!=null && !(endDateString==null || endDateString.isEmpty())){
                DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                endDate = dateFormat.parse(row.get(endDateIndex).toString());
                int noOfRepetetions = dateKeysHelper.getNoOfRepetetions(startDate,endDate,periodType,clientCode);
                if (noOfRepetetions > 0){
                    targetMetadata.put("repeating","yes");
                    targetMetadata.put("noOfRepetetions",noOfRepetetions+"");
                    logger.info("REPETETIONS"+noOfRepetetions);
                }
            }

            logger.info("Granularity, period and dateKeys are " + granularity + ", " + periodType + " &" + dateKey);
        } catch (Exception e){
            logger.info("Error occurred while parsing date"+row.get(startDateIndex)+startDate+e.getMessage());
            e.printStackTrace();
            throw  new ExcelRowException(e.getMessage());
        }

        TargetDefinition targetDefinition=null;
        //Now need to call data layer to create target definition
        try {
            targetDefinition = createTargetDefinition.createTargetDefinition(periodType,
                    targetMetadata.get("fact_id"), clientCode, targetDefinitionRepository);
        } catch (Exception e) {
            throw new ExcelRowException("Error while creating target definition");
        }

        logger.info("Target definition created successfully",targetDefinition.toString());
        targetMetadata.put("def_id", targetDefinition.getId() + "");
        targetMetadata.put("granularity", granularity);
        targetMetadata.put("date_key", dateKey);

        return targetMetadata;
    }

    public HashMap<String, String> getTargetInstanceInfo(String fileName, JSONArray row, JSONArray header, String clientCode)
            throws ExcelRowException {

        logger.info("Fetching the target info now");
        HashMap<String, String> headersMap = (HashMap<String, String>) TargetImportConstants.getHeadersMap().clone();
        HashMap<String, String> assignementTypeMap = TargetImportConstants.getTargetAssignmentMap();
        HashMap<String, String> timePeriodMap = TargetImportConstants.getTimePeriodMap();
        HashMap<String, String> achievementCriteria = TargetImportConstants.getAchievementCalculationRule();
        Set<String> infoHeaders = headersMap.keySet();
        infoHeaders.remove("UserCode");
        infoHeaders.remove("Is the target Repeating?");
        infoHeaders.remove("If target is repeating, then how many times?");

        HashMap<String, String> infoHashMap = new HashMap<>();
        String userCode = "UserCode";
        String val = null;
        //Need to fetch userCode separately as it can be all numeric and sometimes alphanumeric and excel parser doesn't
        //understand the difference
        int ind = header.indexOf(userCode);
        try {
            val = (int) Double.parseDouble(row.get(ind).toString()) + "";
        } catch (Exception e) {
            logger.info("UserCode is string, so handled :)");
            val = row.get(ind).toString();
        }
        infoHashMap.put("userCode", val);

        logger.info("Received header is ", header.toString());
        //fetching values for rest of the columns
        for (String info : infoHeaders) {
            int index = header.indexOf(info);
            logger.info("Index is %d for "+ index+info);
            String value = null;
            if ((info.equals("If target is repeating, then how many times?") && infoHashMap.get("repeating").equals("yes"))
                    || !info.equals("If target is repeating, then how many times?")) {
                value = row.get(index).toString();
            }
            logger.info("Value is "+value+info);
            switch (info) {
                case "TargetLabel":
                    //Must be a string and non-empty
                    if (value == null || value.isEmpty()) {
                        //throw exception
                        logger.info("Found target lable empty", info, value);
                        throw new ExcelRowException("Column <label> cannot be empty");
                    }
                    infoHashMap.put(headersMap.get(info), value);
                    break;
                case "Role":
                    //Must be non-empty or NA
                    if (!(value.isEmpty())) {
                        //set role as empty
                        infoHashMap.put(headersMap.get(info), value);
                    } else {
                        logger.info("Found role empty, setting it as null");
                        infoHashMap.put(headersMap.get(info), null);
                    }
                    break;
                case "TargetAssignment":
                    //The selected value must be among the provided list
                    //Need to have extra check on role column value
                    if (!value.isEmpty()) {
                        String roleIfAny = infoHashMap.get("role");
                        String actualValueAssignmentType = assignementTypeMap.containsKey(value) ? assignementTypeMap.get(value) : null;
                        if (roleIfAny != null && !roleIfAny.equals("NA") && actualValueAssignmentType != null && actualValueAssignmentType.equals("self")) {
                            //throw error saying the assignment type has to be NA if role is specified
                            logger.info("role is not empty, but target assignment is not role_based");
                            throw new ExcelRowException("AssignmentType has cannot be 'self' if role is specified");
                        }
                        if (actualValueAssignmentType == null) {
                            //Throw exception as value is not valid
                            logger.info("the selected value for assignment is not standard", value);
                            throw new ExcelRowException("Value for column AssignmentType is not valid" + value);
                        } else {
                            infoHashMap.put(headersMap.get(info), actualValueAssignmentType);
                        }
                    } else {
                        //Error out as can't be empty
                        logger.info("Target assignment cannot be null");
                        throw new ExcelRowException("Value for column AssignmentType cannot be empty");
                    }
                    break;
                case "TargetValue":
                    //The value must be either double or numeric otherwise reject it
                    if (value != null || !value.isEmpty()) {
                        try {
                            Double targetValue =  Double.parseDouble(value);
                            if (targetValue <= 0) {
                                //throw error
                                logger.info("target value found to be lesser than 0", targetValue);
                                throw new ExcelRowException("TargetValue cannot be less than zero" + value);
                            }
                            infoHashMap.put(headersMap.get(info), targetValue + "");
                        } catch (NumberFormatException e) {
                            logger.info("Target value is not of number type", value);
                            throw new ExcelRowException("Target value is not of number type" + value);
                        }

                    } else {
                        //Error out as can't be empty
                        logger.info("target value cannot be empty");
                        throw new ExcelRowException("Target value cannot be empty");
                    }
                    break;
                case "Period":
                    // The granularity time period for which this target is being set, has to be among the system
                    // supported ones
                    if (value != null || !value.isEmpty()) {
                        String actualPeriod = timePeriodMap.containsKey(value) ? timePeriodMap.get(value) : null;
                        if (actualPeriod == null) {
                            //Throw exception as period is not valid
                            throw new ExcelRowException("The period type is not valid" + value);
                        } else {
                            infoHashMap.put(headersMap.get(info), actualPeriod);
                        }
                    } else {
                        //time period can never be empty
                        throw new ExcelRowException("The period column cannot be left blank" + value);
                    }
                    break;
                case "AchievementCriteria":
                    // The rule can either be self or team
                    String actualCriterria;
                    if (!value.isEmpty()) {
                        actualCriterria = achievementCriteria.containsKey(value) ? achievementCriteria.get(value)
                                : null;
                        if (actualCriterria == null) {
                            //need not throw exception by default we would say it is self
                            actualCriterria = "self";
                        }
                    } else {
                        actualCriterria = "self";
                    }
                    infoHashMap.put(headersMap.get(info), actualCriterria);
                    break;
                case "TargetDesciption(If any)":
                    //This as of now cannot be empty
                    if (value.isEmpty()) {
                        throw new ExcelRowException("target decription cannot be empty");
                    } else {
                        infoHashMap.put(headersMap.get(info), value);
                    }
                    break;
                /*case "Is the target Repeating?":
                    // has to be either of yes/no
                    if (value.isEmpty()) {
                        // No has to be selected , so throw error
                        throw new ExcelRowException("Please select No if target is not repeating");
                    } else {
                        infoHashMap.put(headersMap.get(info), value);
                    }
                    break;
                case "If target is repeating, then how many times?":
                    //this has to be integer type if present
                    if (value == null) {
                        break;
                    }
                    if (!value.isEmpty()) {
                        int indexOfRepeating = header.indexOf("Is the target Repeating?");
                        String repeating = row.get(indexOfRepeating).toString();
                        int repetetions = Integer.parseInt(value);
                        if (repeating.equals("yes") && repetetions > 0) {
                            //valid combination
                            infoHashMap.put(headersMap.get(info), repetetions + "");
                        } else if (repeating.equals("yes") && repetetions <= 0) {
                            //throw exception saying not possible because repetitions has to be @least 1
                            throw new ExcelRowException("No of repetetions has to be greater than 1");
                        } else if (!repeating.equals("yes") && repetetions > 0) {
                            // not possible to give repetitions without saying repeating as yes
                            throw new ExcelRowException("Please mark repeating column as yes if no. of repetetions is greater than zero");
                        }
                    }
                    break;*/
            }
        }
        return infoHashMap;
    }

    public HashMap<String,String> validateAndGetTargetMetadata(String params, String clientCode) throws ExcelRowException {

        //validate fact_id
        Gson gson = new Gson();
        HashMap<String,String> targetMetadata = gson.fromJson(params,new HashMap<String,String>().getClass());
        int mfId=Integer.parseInt(targetMetadata.get("metric_fact_id"));
        if (!clientCode.equals(iFactsMetricsCache.getClientName(mfId))) {
            throw new ExcelRowException("Invalid client params");
        }
        String factId = iFactsMetricsCache.getFactId(mfId);
        targetMetadata.put("fact_id",factId);
        System.out.println("Filters"+targetMetadata);
        //validate dimensions and see if metric fact exists
        if ( targetMetadata.containsKey("metric_fact_id")){
            logger.info("Got fact_id and metric_fact_id in request");
            return targetMetadata;
        } else {
            logger.info("Metadata in import request missing",params);
            throw new ExcelRowException("Missing target metadata parameters");
        }
        //TODO: Remove this hashmap and move it to function, must be accepted from requestBody

    }

    public File createCSV(ArrayList<JSONArray> jsonArrays, String fileName) {
        String csvName = fileName + System.currentTimeMillis() + "-status.csv";
        try {

            FileWriter fileWriter = new FileWriter(csvName);
            for (JSONArray jsonArray : jsonArrays) {
                String currentRowString = "";
                for (int i = 0; i < jsonArray.size(); i++) {
                    currentRowString += jsonArray.get(i) + ",";
                }
                fileWriter.write(currentRowString);
                fileWriter.write("\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            logger.info("Could not create file" + fileName);
        }

        File file = new File(csvName);
        if (file.exists() && file.isFile()){

            return file;
        } else {
            return null;
        }
    }
    public ArrayList<JSONArray> buildResponseData(String msg){
        ArrayList<JSONArray> newL = new ArrayList<JSONArray>();
        JSONArray tmp = new JSONArray();
        tmp.add(msg);
        newL.add(0,tmp);
        return  newL;
    }

    public ResponseEntity<Resource> sendCSV(ArrayList<JSONArray>  jsonArrays, String fileName){
        File csv = createCSV(jsonArrays,fileName);
        try {
            Resource file = new UrlResource(csv.toURI());
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,"filename=\""+csv.getName()+"\"")
                    .body(file);
        } catch (MalformedURLException e){
            e.printStackTrace();
            return null;
        }
    }
}

