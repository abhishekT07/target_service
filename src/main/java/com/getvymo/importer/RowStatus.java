package com.getvymo.importer;

/**
 * Created by abhishek on 06/04/17.
 */
public class RowStatus {
    private Boolean successful;
    private String message;

    public RowStatus() {
    }

    public RowStatus(Boolean successful, String message) {
        this.successful = successful;
        this.message = message;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
