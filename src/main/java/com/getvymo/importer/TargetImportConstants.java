package com.getvymo.importer;

import java.util.HashMap;

/**
 * Created by abhishek on 31/03/17.
 */
public class TargetImportConstants {
    private static HashMap<String, String> headersMap;

    static {
        headersMap = new HashMap<String, String>();
        headersMap.put("TargetLabel", "label");
        headersMap.put("UserCode", "userCode");
        headersMap.put("Role", "role");
        headersMap.put("TargetAssignment", "assignment_type");
        headersMap.put("TargetValue", "target_value");
        headersMap.put("StartDate", "start_date");
        headersMap.put("Period", "period");
        headersMap.put("EndDate", "end_date");
        headersMap.put("AchievementCriteria", "achievement_type");
        headersMap.put("TargetDesciption(If Any)", "description");
        /*headersMap.put("Is the target Repeating?", "repeating");
        headersMap.put("If target is repeating, then how many times?", "noOfRepetetions");*/

    }

    private static HashMap<String, String> targetAssignmentMap;

    static {
        targetAssignmentMap = new HashMap<String, String>();
        targetAssignmentMap.put("NA - when role is non empty and assign to role users", "role_based");
        targetAssignmentMap.put("Assign to this user", "self");
        targetAssignmentMap.put("Assign to all users under him", "all_hierarchy");
        targetAssignmentMap.put("Assign to direct users under him", "direct_hierarchy");
    }

    private static HashMap<String, String> timePeriodMap;

    static {
        timePeriodMap = new HashMap<String, String>();
        timePeriodMap.put("Weekly", "week");
        timePeriodMap.put("Daily", "day");
        timePeriodMap.put("Monthly", "month");
        timePeriodMap.put("Quarterly", "quarter");
        timePeriodMap.put("Yearly", "year");
    }

    private static HashMap<String, String> reverseTimePeriodMap;

    private static HashMap<String, String> achievementCalculationRule;
    static {
        reverseTimePeriodMap = new HashMap<String,String>();
        reverseTimePeriodMap.put("week","Weekly");
        reverseTimePeriodMap.put("day","Daily");
        reverseTimePeriodMap.put("month","Monthly");
        reverseTimePeriodMap.put("quarter","Quarterly");
        reverseTimePeriodMap.put("year","Yearly");
    }
    static {
        achievementCalculationRule = new HashMap<String, String>();
        achievementCalculationRule.put("Targeted user's team has to fulfil target", "team");
        achievementCalculationRule.put("User whom target is assigned has to achieve the target", "self");
    }

    public static HashMap<String, String> getReverseTimePeriodMap() {
        return reverseTimePeriodMap;
    }

    public static HashMap<String, String> getHeadersMap() {
        return headersMap;
    }

    public static HashMap<String, String> getTargetAssignmentMap() {
        return targetAssignmentMap;
    }

    public static HashMap<String, String> getTimePeriodMap() {
        return timePeriodMap;
    }

    public static HashMap<String, String> getAchievementCalculationRule() {
        return achievementCalculationRule;
    }
}
