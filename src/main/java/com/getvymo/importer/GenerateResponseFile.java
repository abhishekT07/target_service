package com.getvymo.importer;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by abhishek on 20/03/17.
 */
public class GenerateResponseFile implements MultipartFile {

    private final String[] rows;
    private final String name;
    public GenerateResponseFile(String[] rows, String name)
    {
        this.rows = rows;
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOriginalFilename() {
        return name;
    }

    @Override
    public String getContentType() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return rows == null || rows.length == 0;
    }

    @Override
    public long getSize() {
        return rows.length;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return new byte[0];
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return null;
    }

    @Override
    public void transferTo(File dest) throws IOException, IllegalStateException {
        new FileOutputStream(dest);
    }
}
