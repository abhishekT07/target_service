package com.getvymo.batch_job;

import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsRoles;
import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.entities.target_entities.TargetStatus;
import com.getvymo.data.sql.entities.org_hierarchy_entities.Users;
import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
import com.getvymo.data.sql.entities.target_entities.UserVsTargetInstance;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsRolesRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetRespository;
import com.getvymo.data.sql.repositories.target_repos.TargetStatusRepository;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsAccessiblehierarchyKeysRepository;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UsersRepository;
import com.getvymo.data.sql.repositories.target_repos.UserVsTargetInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */


@Component
public class TargetScheduler {
    private static Logger logger = LoggerFactory.getLogger(TargetScheduler.class);

    @Autowired
    private TargetInstanceRepository targetInstanceRepository;
    @Autowired
    private UserVsAccessiblehierarchyKeysRepository userVsAccessiblehierarchyKeysRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private TargetRespository targetRespository;
    @Autowired
    private TargetStatusRepository targetStatusRepository;
    @Autowired
    private UserVsTargetInstanceRepository userVsTargetInstanceRepository;
    @Autowired
    private UserVsRolesRepository userVsRolesRepository;



    /*public static void processTargets(TargetStatusRepository targetStatusRepository,
                                      TargetInstanceRepository targetInstanceRepository, TargetRespository targetRespository,
                                      UsersRepository usersRepository, UserVsTargetInstanceRepository
                                              userVsTargetInstanceRepository, UserVsRolesRepository userVsRolesRepository) {*/
    @Scheduled(fixedDelay = 60000)
    public  void processTargets() {

        String incompleteStatus = "INCOMPLETE";

        List<TargetStatus> targetsToProcess = targetStatusRepository.findByStatus(incompleteStatus);
        for (TargetStatus targetStatus : targetsToProcess) {
            try {
                Long uvtIid = targetStatus.getUserVsTargetInstanceId();
                System.out.println("repos "+uvtIid);
                UserVsTargetInstance userVsTargetInstance = userVsTargetInstanceRepository.findOne(uvtIid);
                System.out.println("instances"+userVsTargetInstance);
                TargetInstance targetInstance = targetInstanceRepository.findById(
                        userVsTargetInstance.getTargetInstanceId());


                String userCode = userVsTargetInstance.getUserCode();
                String clientCode = targetInstance.getClientCode();
                String role = userVsTargetInstance.getRole();
                String assignmentType = userVsTargetInstance.getAssignmentType();
                List<UserVsTarget> listToSave = new ArrayList<UserVsTarget>();

                if (role.equals("NA") || assignmentType.equals("self")) {
                    List<Users> targetUsers = null;
                    if (assignmentType.equals("self")) {
                        targetUsers = usersRepository.getByUserIdAndClientIdAndDisabled(userCode, clientCode,0);
                    } else if (assignmentType.equals("all_hierarchy")) {
                        targetUsers = usersRepository.getUsers(userCode, clientCode);
                    } else if (assignmentType.equals("direct_hierarchy")) {
                        targetUsers = usersRepository.getDirectHierarchy(userCode, clientCode);
                    }

                    System.out.println("\nUSERLISTLENGTH" + targetUsers.size());

                    for (Users user : targetUsers) {

                        listToSave.add(new UserVsTarget(user.getUserId(), user.getHierarchyKey(),
                                userVsTargetInstance.getTargetInstanceId(), targetInstance.getMetricFactId(),
                                targetInstance.getDateKey(), targetInstance.getClientCode(),
                                (int) Double.parseDouble(userVsTargetInstance.getTargetValue() + ""),0));
                    }
                } else {
                    List<UserVsRoles> userVsRoles = null;
                    if (assignmentType.equals("role_based")){
                        userVsRoles = userVsRolesRepository.getUsersWithRole(userCode,clientCode,role);
                    } else if (assignmentType.equals("all_hierarchy")){
                        userVsRoles = userVsRolesRepository.getUsersHierarchyWithRole(userCode,clientCode,role);
                    } else if (assignmentType.equals("direct_hierarchy")){
                        userVsRoles = userVsRolesRepository.getDirectHierarchyWithRole(userCode,clientCode,role);
                    }

                    System.out.println("\nUSERLISTLENGTH" + userVsRoles.size());
                    for (UserVsRoles userRole : userVsRoles) {

                        listToSave.add(new UserVsTarget(userRole.getUserCode(), userRole.getHierarchyKey(),
                                userVsTargetInstance.getTargetInstanceId(), targetInstance.getMetricFactId(),
                                targetInstance.getDateKey(), targetInstance.getClientCode(),
                                (int) Double.parseDouble(userVsTargetInstance.getTargetValue() + ""),0));
                    }
                }

//                targetRespository.save(listToSave);
                Boolean bool = findAndSave(listToSave,targetRespository);
                targetStatus.setStatus("COMPLETE");
                targetStatusRepository.save(targetStatus);


            } catch (Exception e){
                logger.info("Exception occured while executing scheduler"+e);
                e.printStackTrace();
            }

        }
    }

    public static boolean findAndSave(List<UserVsTarget> userVsTargets, TargetRespository targetRespository) {
        for (UserVsTarget userVsTarget : userVsTargets) {
            UserVsTarget found = targetRespository.findByUserCodeAndInstanceIdAndMetricFactId(
                    userVsTarget.getUserCode(), userVsTarget.getInstanceId(),
                    userVsTarget.getMetricFactId());
            if (found != null) {
                logger.info("found userVsTarget, updating one");
                if (found.getHierarchyKey().equals(userVsTarget.getHierarchyKey())) {
                    targetRespository.updateTargetValue(userVsTarget.getValue(), userVsTarget.getUserCode(),
                            userVsTarget.getMetricFactId(), userVsTarget.getInstanceId(), userVsTarget.getHierarchyKey());
                } else {
                    targetRespository.updateTargetValueAndHKey(userVsTarget.getValue(), userVsTarget.getUserCode(),
                            userVsTarget.getMetricFactId(), userVsTarget.getInstanceId(), userVsTarget.getHierarchyKey());
                }
            } else {
                targetRespository.save(userVsTarget);
            }
        }
        return true;
    }
}
