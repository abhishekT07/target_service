package com.getvymo.cache.impl;

import com.getvymo.cache.AbstractRepositoryFactory;
import com.getvymo.cache.IDateKeyCache;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
@Component
public class DateKeyFactory implements AbstractRepositoryFactory {
	@Autowired
	private DateKeyCacheImpl object;

	@Override
	public IDateKeyCache getDateKeyCache() {
		
		//IDateKeyCacheImpl dateKeyChace=new IDateKeyCacheImpl();
		return object;
	}
	

}
