package com.getvymo.cache.impl;

import java.sql.Date;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

import com.getvymo.data.sql.entities.target_entities.DateKeys;

public class ClientDateKeyCache {
	private Map<String, List<String>> dateVsDateKeyMap;
	private Map<String, List<Date>> dateKeyVsDateMap;
	private String clientId;
	public ClientDateKeyCache(String clientId){
		this.clientId=clientId;
		this.dateVsDateKeyMap = new HashMap<>();
		this.dateKeyVsDateMap = new HashMap<>();

	}

	public Map<String, List<String>> getDateVsDateKeyMap() {
		return dateVsDateKeyMap;
	}
	public void setDateVsDateKeyMap(Map<String, List<String>> dateVsDateKeyMap) {
		this.dateVsDateKeyMap = dateVsDateKeyMap;
	}
	public Map<String, List<Date>> getDateKeyVsDateMap() {
		return dateKeyVsDateMap;
	}
	public void setDateKeyVsDateMap(Map<String, List<Date>> dateKeyVsDateMap) {
		this.dateKeyVsDateMap = dateKeyVsDateMap;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
