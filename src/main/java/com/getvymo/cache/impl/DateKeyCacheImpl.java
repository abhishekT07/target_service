package com.getvymo.cache.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.getvymo.cache.IDateKeyCache;

import com.getvymo.cache.schedular.DateKeysRefreshTask;
//import com.getvymo.cache.schedular.ScheduledTasks;

import com.getvymo.data.sql.repositoryHelper.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.LoggerFactory;

@Component
// @Scope("prototype")
public class DateKeyCacheImpl implements IDateKeyCache {

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(DateKeyCacheImpl.class);
	@Autowired
	private DateKeysRefreshTask scheduledTask;

	@Override
	public List<String> getLiveKeys(String clientId,Date date) {
		ClientDateKeyCache cacheObj = scheduledTask.getClientVsDateKeyMap(clientId);
		if(cacheObj==null){
			logger.error("Could not find date keys for client "+clientId+" ");
			return null;
		} else {
			logger.debug("Found keys for date "+date.toString()+" and client "+clientId+" : "+cacheObj.getDateVsDateKeyMap().get(date.toString()));
			return cacheObj.getDateVsDateKeyMap().get(date.toString());
		}
	}

	@Override
	public List<Date> getStartAndEndDate(String clientId,String dateKey) {
		ClientDateKeyCache cacheObj = scheduledTask.getClientVsDateKeyMap(clientId);
		if(cacheObj==null){
			logger.error("Could not find start and end dates for client "+clientId);
			return null;
		} else {
			logger.debug("Found start/end dates for key "+dateKey+" and client "+clientId+" : "+cacheObj.getDateKeyVsDateMap().get(dateKey));
			return cacheObj.getDateKeyVsDateMap().get(dateKey);
		}
	}

	@Override
	public List<String> getKeysForDateRange(String clientId,Date startDate,Date endDate){
		List<String> dateKeys = new ArrayList<>();
		Date date = startDate;
		ClientDateKeyCache cacheObj = scheduledTask.getClientVsDateKeyMap(clientId);
		if(cacheObj==null){
			logger.error("Could not find date keys for client "+clientId+" ");
			return null;
		} else {
			while(date.compareTo(endDate)<=0){
				List<String> keys = cacheObj.getDateVsDateKeyMap().get(date.toString());
				if(keys!=null){
					dateKeys.addAll(keys);
				}
				date = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			}
		}
		return dateKeys;
	}

	@Override
	public List<String> getKeysForPeriodAndDateRange(String clientId, Date startDate, Date endDate, String period) {
		List<String> dateKeys = new ArrayList<>();
		Date date = startDate;
		int periodIndex = Util.getIndexOfDateKey(period);
		ClientDateKeyCache cacheObj = scheduledTask.getClientVsDateKeyMap(clientId);
		if(cacheObj==null){
			logger.error("Could not find date keys for client "+clientId+" ");
			return null;
		} else {
			while(date.compareTo(endDate)<=0){
				List<String> keys = cacheObj.getDateVsDateKeyMap().get(date.toString());
				if(keys!=null){
					dateKeys.add(keys.get(periodIndex));
				}
				date = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			}
		}
		return dateKeys;
	}

	@Override
	public List<String> getFutureDateKeys(String clientId) {
		ArrayList<String> dateKeys = new ArrayList<>();
		Date date = new Date(System.currentTimeMillis());
		ClientDateKeyCache clientDateKeyCache = scheduledTask.getClientVsDateKeyMap(clientId);
		if (clientDateKeyCache==null){
			logger.error("Could not find date keys for client "+ clientId);
			return null;
		} else {
			logger.info("client dates"+clientDateKeyCache.getDateVsDateKeyMap().get(date.toString()));
			while(clientDateKeyCache.getDateVsDateKeyMap().get(date.toString())!=null){
				List<String> keys = clientDateKeyCache.getDateVsDateKeyMap().get(date.toString());
				if(keys!=null){
					dateKeys.addAll(keys);
				}
				date = new Date(date.getTime() + (1000 * 60 * 60 * 24));
			}
		}
		return dateKeys;
	}

	//	@Override
//	public HashMap<String, List<Date>> getStartAndEndDate() {
//		// TODO Auto-generated method stub
//		// ScheduledTask job=new ScheduledTask();
//		TreeMap<Date, DateKeys> datesMap = scheduledTask.dateCache();
//		HashMap<DateKeys, Date> startEndMap = new HashMap<DateKeys, Date>();
//		HashMap<String, List<Date>> resultMap = new HashMap<String, List<Date>>();
//
//		for (Entry<Date, DateKeys> entry : datesMap.entrySet()) {
//			startEndMap.put(entry.getValue(), entry.getKey());
//		}
//		for (Entry<Date, DateKeys> entry : datesMap.entrySet()) {
//
//			List<Date> dateList = new ArrayList<Date>(2);
//
//			if (resultMap.isEmpty()) {
//
//				if ((entry.getValue().getWeekKey() != null)) {
//					dateList.add(0, entry.getKey());
//					resultMap.put(entry.getValue().getWeekKey(), dateList);
//				}
//				if ((entry.getValue().getMonthKey() != null)) {
//					dateList.add(0, entry.getKey());
//					resultMap.put(entry.getValue().getMonthKey(), dateList);
//				}
//				if ((entry.getValue().getYearKey() != null)) {
//					dateList.add(0, entry.getKey());
//					resultMap.put(entry.getValue().getYearKey(), dateList);
//				}
//			}
//
//			if ((entry.getValue().getDayKey() != null) ) {
//				List<Date> tempDay=new ArrayList<Date>();
//				tempDay.add(entry.getKey());
//				tempDay.add(entry.getKey());
//				resultMap.put(entry.getValue().getDayKey(), tempDay);
//			}
//			 if ((entry.getValue().getWeekKey() != null) && (resultMap.get(entry.getValue().getWeekKey()) != null)) {
//				List<Date> tempDate = resultMap.get(entry.getValue().getWeekKey());
//
//
//
//				tempDate.set(1, entry.getKey());
//
//				resultMap.put(entry.getValue().getWeekKey(), tempDate);
//			}
//			 if ((entry.getValue().getMonthKey() != null) && (resultMap.get(entry.getValue().getMonthKey()) != null)) {
//				// dateList.
//				List<Date> tempDate = resultMap.get(entry.getValue().getMonthKey());
//
//
//				tempDate.set(1, entry.getKey());
//
//				resultMap.put(entry.getValue().getMonthKey(), tempDate);
//			}
//			 if ((entry.getValue().getYearKey() != null)
//					&& (resultMap.get(entry.getValue().getYearKey()) != null)) {
//				List<Date> tempDate = resultMap.get(entry.getValue().getYearKey());
//
//
//
//				tempDate.set(1, entry.getKey());
//
//				resultMap.put(entry.getValue().getYearKey(), tempDate);
//
//			}
//
//
//
//		}
//
//		return resultMap;
//	}

}
