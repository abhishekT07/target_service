package com.getvymo.cache;

public interface AbstractRepositoryFactory {
	public IDateKeyCache getDateKeyCache();

}
