package com.getvymo.cache.facts;

import com.getvymo.data.response.entities.Metadata;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * Created by abhishek on 10/04/17.
 */
@Service
public interface IFactsCache {

    public String getFactValue(String factId);
    public String getFactModule(String factId);
    public String getDimensionId(String factId);
    public String getFactMetric(String factId);
    public String getTargetType(String factId);
    public String getTargetFactDesc(String factId);
    public Metadata getFactMetadata(int  metricFactId);
}
