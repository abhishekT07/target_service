package com.getvymo.cache.facts;

import com.getvymo.data.response.entities.Metadata;
import com.getvymo.cache.fact_metrics.FactsMetricsCacheImpl;
import com.getvymo.cache.fact_metrics.IFactsMetricsCache;
import com.getvymo.cache.scheduler.FactsScheduler;
import com.getvymo.data.sql.entities.target_entities.Facts;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import java.util.HashMap;

/**
 * Created by abhishek on 10/04/17.
 */
@Controller
public class FactsCacheImpl implements IFactsCache {
    Logger logger = LoggerFactory.getLogger(FactsCacheImpl.class);
    @Override
    public String getFactValue(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return requiredFact.getState();
    }

    @Override
    public String getFactModule(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return  requiredFact.getModule();
    }

    @Override
    public String getDimensionId(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return requiredFact.getDimensionId();
    }

    @Override
    public String getFactMetric(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return requiredFact.getMetric();
    }

    @Override
    public String getTargetType(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return requiredFact.getTargetType();
    }

    @Override
    public String getTargetFactDesc(String factId) {
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(factId);
        return requiredFact.getDesc();
    }

    @Override
    public Metadata getFactMetadata(int mfID) {
        IFactsMetricsCache iFactsMetricsCache = new FactsMetricsCacheImpl();
        HashMap<String,Facts> factsHashMap = FactsScheduler.getFactsList();
        Facts requiredFact = factsHashMap.get(iFactsMetricsCache.getFactId(mfID));
        Gson gson = new Gson();
        Metadata jsonObject = null;
        try {
            logger.info("METADATA is"+requiredFact.getMetadata());
           jsonObject = gson.fromJson(requiredFact.getMetadata(),Metadata.class);
        }catch (Exception e){
            e.printStackTrace();
            logger.info("exception is"+e.getMessage());
        }

        return jsonObject;
    }
}
