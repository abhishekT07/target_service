package com.getvymo.cache.scheduler;

import com.getvymo.data.sql.entities.target_entities.Facts;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 10/04/17.
 */

@Component
public class FactsScheduler {
    private static Logger logger = LoggerFactory.getLogger(FactsScheduler.class);

    @Autowired
    FactsRepository factsRepository;
    private static HashMap<String,Facts> factsList;

    public static HashMap<String, Facts> getFactsList() {
        return factsList;
    }

    private static void setFactsList(HashMap<String, Facts> factsList) {
        FactsScheduler.factsList = factsList;
    }

    @Scheduled(fixedRate = 500000)
    public void updateFactsCache(){
        logger.info("Inside Facts Scheduler"+ System.currentTimeMillis());
        HashMap<String,Facts> factsHashMap = new HashMap<String,Facts>();

        List<Facts> factss = (List<Facts>) factsRepository.findAll();

        for (Facts fact:
             factss) {
            factsHashMap.put(fact.getId(),fact);
        }
        FactsScheduler.setFactsList(factsHashMap);
    }
}
