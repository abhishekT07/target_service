package com.getvymo.cache.scheduler;

import com.getvymo.data.response.entities.MetricFacts;
import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import com.getvymo.data.sql.entities.target_entities.Facts;
import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 10/04/17.
 */

@Component
public class MetricFactsScheduler {
    private static Logger logger = LoggerFactory.getLogger(FactsScheduler.class);

    @Autowired
    FactsMetricsRepository factsMetricsRepository;
    private static HashMap<Integer,ArrayList<FactMetrics>> factsMetricsList;
    private static HashMap<String,ArrayList<FactMetrics>> clientFactMetricsMap;

    public static HashMap<Integer, ArrayList<FactMetrics>> getFactsMetricsList() {
        return factsMetricsList;
    }

    private static void setFactsMetricsList(HashMap<Integer,ArrayList<FactMetrics>> factsMetricsList) {
        MetricFactsScheduler.factsMetricsList = factsMetricsList;
    }

    public static HashMap<String, ArrayList<FactMetrics>> getClientFactMetricsMap() {
        return clientFactMetricsMap;
    }

    private static void setClientFactMetricsMap(HashMap<String, ArrayList<FactMetrics>> clientFactMetricsMap) {
        MetricFactsScheduler.clientFactMetricsMap = clientFactMetricsMap;
    }

    @Scheduled(fixedRate = 500000)
    public void updateFactsCache() {
        logger.info("Inside Metric Facts Scheduler" + System.currentTimeMillis());
        HashMap<Integer, ArrayList<FactMetrics>> factsMetricHashMap = new HashMap<Integer, ArrayList<FactMetrics>>();
        HashMap<String, ArrayList<FactMetrics>> clientMap = new HashMap<>();
        List<FactMetrics> factss = (List<FactMetrics>) factsMetricsRepository.findAll();

        for (FactMetrics metricFact :
                factss) {
            //by mf id
            if (!factsMetricHashMap.containsKey(metricFact.getId())) {
                ArrayList<FactMetrics> factMetricss = new ArrayList<>();
                factMetricss.add(metricFact);
                factsMetricHashMap.put(metricFact.getId(), factMetricss);
            } else {
                ArrayList<FactMetrics> factMetricss = factsMetricHashMap.get(metricFact.getId());
                factMetricss.add(metricFact);
                factsMetricHashMap.put(metricFact.getId(), factMetricss);
            }
            //by client code
            if (!clientMap.containsKey(metricFact.getClientCode())) {
                ArrayList<FactMetrics> factMetricss = new ArrayList<>();
                factMetricss.add(metricFact);
                clientMap.put(metricFact.getClientCode(), factMetricss);
            } else {
                ArrayList<FactMetrics> factMetricss = clientMap.get(metricFact.getClientCode());
                factMetricss.add(metricFact);
                clientMap.put(metricFact.getClientCode(), factMetricss);
            }

        }
        logger.info("Done retrieving factsMetrics" + factsMetricHashMap.keySet());
        MetricFactsScheduler.setFactsMetricsList(factsMetricHashMap);
        MetricFactsScheduler.setClientFactMetricsMap(clientMap);

    }
}
