package com.getvymo.cache;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

public interface IDateKeyCache {
	
	public List<String> getLiveKeys(String clientId,Date date);
	public List<Date> getStartAndEndDate(String clientId,String dateKey);
	public List<String> getKeysForDateRange(String clientId,Date startDate,Date endDate);
	public List<String> getFutureDateKeys(String clientId);
	public List<String> getKeysForPeriodAndDateRange(String clientId, Date startDate, Date endDate, String period);

}
