package com.getvymo.cache.schedular;

import java.util.*;
import java.sql.Date;

// Create a class extends with TimerTask


import com.getvymo.cache.impl.ClientDateKeyCache;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import com.getvymo.data.sql.entities.target_entities.DateKeys;
import com.getvymo.data.sql.repositories.target_repos.TargetRepositoryDate;
import com.getvymo.targets.data.api.DataReadImpl;

@Component
public class DateKeysRefreshTask {
	
	@Autowired
    private TargetRepositoryDate targetDateRepository;

	private Map<String, ClientDateKeyCache> clientVsDateKeyMap ;

	public ClientDateKeyCache getClientVsDateKeyMap(String clientId) {
		if(clientVsDateKeyMap==null){
			reloadCache();
		}
		return clientVsDateKeyMap.get(clientId);
	}
//
//	public void setDateKeyMap(TreeMap<Date, DateKeys> dateKeyMap) {
//		this.dateKeyMap = dateKeyMap;
//	}

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(DataReadImpl.class);

  //  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 500000)
    private void reloadCache() {
    	
    	logger.debug("reloading date key cache");
  //  	logger.info("The time is now {}", dateFormat.format(new Date()));
        
        List<DateKeys> dateKeys = (List<DateKeys>) targetDateRepository.findAllByOrderByDateKeysPKAsc();
		Map<String, ClientDateKeyCache> clientVsDateKeyMapNew = new HashMap<>();
		for (DateKeys datekey : dateKeys) {
			String clientId = datekey.getDateKeysPK().getClientCode();
			if(clientVsDateKeyMapNew.get(clientId)==null){
				clientVsDateKeyMapNew.put(clientId,new ClientDateKeyCache(clientId) );
			}

			ClientDateKeyCache cacheObj = clientVsDateKeyMapNew.get(clientId);
			logger.debug("Cache state for client "+clientId+" DateVsDateKeyMap-> "+cacheObj.getDateVsDateKeyMap().keySet().size()+" DateKeyVsDateMap-> "+cacheObj.getDateKeyVsDateMap().keySet().size());
			String dateString = datekey.getDateKeysPK().getId().toString();
			List<String> dateKeysList = getDateKeyList(datekey);
			cacheObj.getDateVsDateKeyMap().put(dateString, dateKeysList);
			populateDateKeyCache(cacheObj,datekey);
		}
		this.clientVsDateKeyMap = clientVsDateKeyMapNew;
    }


    private List<String> getDateKeyList(DateKeys dateKey){
		List<String> dateKeysList = new ArrayList<>();
		dateKeysList.add(dateKey.getDayKey());
		dateKeysList.add(dateKey.getWeekKey());
		dateKeysList.add(dateKey.getMonthKey());
		dateKeysList.add(dateKey.getQuarterKey());
		dateKeysList.add(dateKey.getYearKey());
		return dateKeysList;
	}

	private void populateDateKeyCache(ClientDateKeyCache cacheObj,DateKeys datekey){
		String dayKey = datekey.getDayKey();
		String weekKey = datekey.getWeekKey();
		String monthKey = datekey.getMonthKey();
		String yearKey = datekey.getYearKey();
		String quarterKey = datekey.getQuarterKey();
		processDataKey(cacheObj,dayKey,datekey.getDateKeysPK().getId());
		processDataKey(cacheObj,weekKey,datekey.getDateKeysPK().getId());
		processDataKey(cacheObj,monthKey,datekey.getDateKeysPK().getId());
		processDataKey(cacheObj,quarterKey,datekey.getDateKeysPK().getId());
		processDataKey(cacheObj,yearKey,datekey.getDateKeysPK().getId());

	}

	private void processDataKey(ClientDateKeyCache cacheObj,String key,Date date){
		if(cacheObj.getDateKeyVsDateMap().get(key)==null){
			List<Date> dateList = new ArrayList<>();
			dateList.add(date);
			dateList.add(date);
			cacheObj.getDateKeyVsDateMap().put(key,dateList);
		} else {
			List<Date> dateList = cacheObj.getDateKeyVsDateMap().get(key);
			dateList.remove(1);
			dateList.add(date);
		}
	}
}