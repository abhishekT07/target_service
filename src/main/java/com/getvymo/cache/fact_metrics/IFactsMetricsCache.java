package com.getvymo.cache.fact_metrics;

import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by abhishek on 10/04/17.
 */
@Service
public interface IFactsMetricsCache {
    public ArrayList<FactMetrics> getFactMetrics(int metricFactId);
    public String getFactId(int metricFactId);
    public String getMetricFactsDescription(int metricFactId);
    public ArrayList<FactMetrics> getAllMetricFactsByClient(String clientCode);
    public String getClientName(int metricFactid);
}
