package com.getvymo.cache.fact_metrics;

import com.getvymo.cache.scheduler.MetricFactsScheduler;
import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abhishek on 10/04/17.
 */
@Controller
public class FactsMetricsCacheImpl implements IFactsMetricsCache {
    @Override
    public String getFactId(int metricFactId) {
        HashMap<Integer,ArrayList<FactMetrics>> integerArrayListHashMap = MetricFactsScheduler.getFactsMetricsList();
        ArrayList<FactMetrics> factMetricsArrayList = integerArrayListHashMap.get(metricFactId);
        return factMetricsArrayList.get(0).getFactId();
    }

    @Override
    public ArrayList<FactMetrics> getFactMetrics(int metricFactId) {
        HashMap<Integer,ArrayList<FactMetrics>> integerArrayListHashMap = MetricFactsScheduler.getFactsMetricsList();
        ArrayList<FactMetrics> factMetricsArrayList = integerArrayListHashMap.get(metricFactId);
        return factMetricsArrayList;
    }

    @Override
    public String getMetricFactsDescription(int metricFactId) {
        HashMap<Integer,ArrayList<FactMetrics>> integerArrayListHashMap = MetricFactsScheduler.getFactsMetricsList();
        ArrayList<FactMetrics> factMetricsArrayList = integerArrayListHashMap.get(metricFactId);
        String mfDesc = factMetricsArrayList.size() > 0 ? factMetricsArrayList.get(0).getDesc() : null;
        return mfDesc;
    }

    @Override
    public ArrayList<FactMetrics> getAllMetricFactsByClient(String clientCode) {
        HashMap<String,ArrayList<FactMetrics>> integerArrayListHashMap = MetricFactsScheduler.getClientFactMetricsMap();
        ArrayList<FactMetrics> clientList = integerArrayListHashMap.get(clientCode);
        return clientList;
    }

    @Override
    public String getClientName(int metricFactid) {
        HashMap<Integer,ArrayList<FactMetrics>> integerArrayListHashMap = MetricFactsScheduler.getFactsMetricsList();
        ArrayList<FactMetrics> factMetricsArrayList = integerArrayListHashMap.get(metricFactid);
        String clientCode = factMetricsArrayList.size() > 0 ? factMetricsArrayList.get(0).getClientCode() : null;
        return clientCode;
    }
}
