package com.getvymo.data.sql.repositoryHelper;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.*;

public class CustomResponse<T> {
	
	private List<T> targets;
	private Set<T> levelTargets;
//	@JsonProperty("targets_value")
//	public Set<T> getLevelTargets() {
//		return levelTargets;
//	}
//	@JsonProperty("targets_value")
//	public void setLevelTargets(Set<T> levelTargets) {
//		this.levelTargets = levelTargets;
//	}
	private Integer count;
	
	
	public CustomResponse(List<T> targets) {
		super();
		this.targets = targets;
		this.count = this.targets.size();
	}
	
	public CustomResponse(Set<T> levelTargets) {
		super();
		this.levelTargets = levelTargets;
		this.count = this.levelTargets.size();
	}
	@JsonProperty("targets")
	public List<T> getList() {
		return targets;
	}
	@JsonProperty("targets")
	public void setList(List<T> targets) {
		this.targets = targets;
		count = this.targets.size();
	}
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}
	@JsonProperty("count")
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}
