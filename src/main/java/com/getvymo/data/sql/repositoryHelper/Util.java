package com.getvymo.data.sql.repositoryHelper;

import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsAccessibleHierarchyKeys;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.slf4j.LoggerFactory;
import com.getvymo.data.sql.entities.target_entities.DateKeys;
import com.getvymo.data.sql.repositories.target_repos.TargetRepositoryDate;

/**
 * Created by abhishek on 20/03/17.
 */
public class Util {
	@PersistenceContext
	private static EntityManager entityManager;
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(Util.class);

	public static String[] getHierarchyKeys(List<UserVsAccessibleHierarchyKeys> accessibleHierarchyKeys) {
		String[] s = new String[accessibleHierarchyKeys.size()];
		int i = 0;
		for (UserVsAccessibleHierarchyKeys row : accessibleHierarchyKeys) {
			s[i++] = row.getHierarchyKey();
		}
		return s;
	}

	public static List<Date> findDates(String userId) {

		List<Date> date = new ArrayList<Date>();
		System.out.println("USERID" + userId);
		String sqlStartDate = "SELECT d.id  as startDate FROM DateKeys d, UserVsTarget u WHERE (d.dayKey=u.dateKey OR d.weekKey=u.dateKey OR d.monthKey=u.dateKey OR d.yearKey=u.dateKey) and u.userCode='"
				+ userId + "' order by d.id asc";
		List<Date> getAllKeys = entityManager.createQuery(sqlStartDate).getResultList();
		String sqlEndDate = "SELECT d.id as EndDate FROM DateKeys d, UserVsTarget u WHERE (d.dayKey=u.dateKey OR d.weekKey=u.dateKey OR d.monthKey=u.dateKey OR d.yearKey=u.dateKey) and u.userCode='"
				+ userId + "' order by d.id desc";
		List<Date> getAllEndKeys = entityManager.createQuery(sqlEndDate).getResultList();

		date.add(getAllKeys.get(0));
		date.add(getAllEndKeys.get(0));
		return date;
	}

	public static TreeMap<Date, DateKeys> dateKeysStore(TargetRepositoryDate dateTargetRepository) {
		logger.info("compute date keys map");
		List<DateKeys> dateKeys = (List<DateKeys>) dateTargetRepository.findAll();
		TreeMap<Date, DateKeys> dateKeyMap = new TreeMap<Date, DateKeys>();
		for (DateKeys datekey : dateKeys) {
			dateKeyMap.put(datekey.getDateKeysPK().getId(), datekey);
		}
		return dateKeyMap;
	}

	public static HashMap<String,ArrayList<Date>> buildStartEndDateList(TreeMap<Date, DateKeys> dateKeysTreeMap){
		HashMap<String,ArrayList<Date>> keyBasedDate = new HashMap<String,ArrayList<Date>>();

		for (DateKeys datekey : dateKeysTreeMap.values()) {
			//for weekKey
			String weekKey = datekey.getWeekKey();
			if(keyBasedDate.containsKey(weekKey)){
				ArrayList<Date> fetchList = keyBasedDate.get(weekKey);
				if (fetchList.size()==2){
					if (fetchList.get(0).toLocalDate().isAfter(datekey.getDateKeysPK().getId().toLocalDate())){
						fetchList.set(0,datekey.getDateKeysPK().getId());
					} else if (fetchList.get(0).toLocalDate().isBefore(datekey.getDateKeysPK().getId().toLocalDate()) &&
							fetchList.get(1).toLocalDate().isBefore(datekey.getDateKeysPK().getId().toLocalDate())){
						fetchList.set(1,datekey.getDateKeysPK().getId());
					}
				} else {
					if (fetchList.get(0).toLocalDate().isAfter(datekey.getDateKeysPK().getId().toLocalDate())){
						fetchList.add(0,datekey.getDateKeysPK().getId());
					} else {
						fetchList.add(1,datekey.getDateKeysPK().getId());
					}
				}
			} else {
				ArrayList<Date> dates = new ArrayList<Date>();
				dates.add(0,datekey.getDateKeysPK().getId());
				keyBasedDate.put(datekey.getWeekKey(),dates);
			}
		}
		return keyBasedDate;
	}

	public static int getIndexOfDateKey(String dateKey){
		List<String> list = new ArrayList<>();
		list.add("day");
		list.add("week");
		list.add("month");
		list.add("quarter");
		list.add("year");
		return list.indexOf(dateKey);
	}

//	public static List<ActiveTargets> findStartandEndDate(List<ActiveTargets> activeTargets,
//			TreeMap<Date, DateKeys> dateKeyMap) {
//		logger.info("compute start and end date");
//		Date sqlDate = new Date(System.currentTimeMillis());
//		Date startDate = null;
//		Date endDate = null;
//		List<ActiveTargets> activeTargetsList = new ArrayList<ActiveTargets>();
//		for (ActiveTargets activeTarget : activeTargets) {
//			logger.info("inside active targets");
//			for (Entry<Date, DateKeys> dateKey : dateKeyMap.entrySet()) {
//				DateKeys dateKeyObject = dateKey.getValue();
//				logger.info("start date" + dateKeyObject.getId() + "..." + sqlDate + ".."
//						+ dateKeyObject.getId().getTime() + ".." + System.currentTimeMillis());
//				// if
//				// (dateKeyObject.getId().toString().equals(sqlDate.toString()))
//				// {
//				logger.info("inside equals");
//				if (activeTarget.getDatekey().equals(dateKeyObject.getDayKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getWeekKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getMonthKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getYearKey())) {
//					if (startDate == null && endDate == null) {
//						startDate = dateKeyObject.getId();
//						endDate = startDate;
//						logger.info("start date" + startDate);
//					}
//
//					if ((endDate.compareTo(dateKeyObject.getId()) < 0)
//							|| (endDate.compareTo(dateKeyObject.getId()) == 0)) {
//						endDate = dateKeyObject.getId();
//						logger.info("end date" + endDate);
//					}
//
//				
//				}
//				}
//			// }
//			activeTarget.setStartDate(startDate);
//			activeTarget.setEndDate(endDate);
//			activeTargetsList.add(activeTarget);
//
//		}
//		return activeTargetsList;
//	}

//	public static List<ActiveTargets> targetsStartAndEndDate(List<ActiveTargets> activetargets,Date startDate,Date endDate,TreeMap<Date, DateKeys> dateKeyMap) {
//		logger.info("inside date-range util");
//		Date sqlDate = new Date(System.currentTimeMillis());
//		
//		List<ActiveTargets> activeTargetsList = new ArrayList<ActiveTargets>();
//		for (ActiveTargets activeTarget : activetargets) {
//			for (Entry<Date, DateKeys> dateKey : dateKeyMap.entrySet()) {
//				DateKeys dateKeyObject = dateKey.getValue();
//			
//			if ((dateKeyObject.getId().compareTo(startDate)>=0)&&(dateKeyObject.getId().compareTo(endDate)<=0))
//			{
//				logger.info("inside equals");
//				if (activeTarget.getDatekey().equals(dateKeyObject.getDayKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getWeekKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getMonthKey())
//						|| activeTarget.getDatekey().equals(dateKeyObject.getYearKey())) {
//					if (startDate == null && endDate == null) {
//						startDate = dateKeyObject.getId();
//						endDate = startDate;
//						logger.info("start date" + startDate);
//					}
//
//					if ((endDate.compareTo(dateKeyObject.getId()) < 0)
//							|| (endDate.compareTo(dateKeyObject.getId()) == 0)) {
//						endDate = dateKeyObject.getId();
//						logger.info("end date" + endDate);
//					}
//
//				}
//			}
//			}
//			activeTarget.setStartDate(startDate);
//			activeTarget.setEndDate(endDate);
//			activeTargetsList.add(activeTarget);
//
//		}
//		return activeTargetsList;
//}
}
