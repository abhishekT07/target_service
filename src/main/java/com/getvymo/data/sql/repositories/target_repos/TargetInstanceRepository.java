package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Column;
import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */

@Repository
public interface TargetInstanceRepository extends CrudRepository<TargetInstance,String>{
    public TargetInstance findById(int id);

    public TargetInstance findByClientCodeAndMetricFactIdAndDateKeyAndAchievementType(String
                          clientCode, int metricFactId, String dateKey, String achievementType);

    public List<TargetInstance> findByClientCode(String clientCode);
}
