package com.getvymo.data.sql.repositories.org_hierarchy_repos;

import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsAccessibleHierarchyKeys;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */


@Repository
public interface UserVsAccessiblehierarchyKeysRepository extends CrudRepository<UserVsAccessibleHierarchyKeys,Long> {
    public List<UserVsAccessibleHierarchyKeys> findByUserId(String userId);
    public List<UserVsAccessibleHierarchyKeys> findByUserIdAndClientId(String userId, String clientId);
}
