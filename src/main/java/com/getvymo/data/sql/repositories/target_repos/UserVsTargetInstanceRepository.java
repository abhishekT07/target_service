package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.UserVsTargetInstance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */
@Repository
public interface UserVsTargetInstanceRepository extends CrudRepository<UserVsTargetInstance,Long> {

    public UserVsTargetInstance findByUserCodeAndRoleAndDateKeyAndClientCodeAndTargetInstanceId(String userCode, String role, String dateKey, String clientCode, int instanceId);

    public List<UserVsTargetInstance> findByTargetInstanceId(int instanceId);

    public List<UserVsTargetInstance> findByClientCode(String clientCode);

}
