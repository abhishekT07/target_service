package com.getvymo.data.sql.repositories.org_hierarchy_repos;

import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsRoles;
import com.getvymo.data.sql.entities.org_hierarchy_entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by abhishek on 03/04/17.
 */
public interface UserVsRolesRepository extends CrudRepository<UserVsRoles, String> {

    @Query(value = "SELECT uvr.userId, uvr.hierarchyKey, uvr.clientId, uvr.role FROM org_hierarchy_service.userVsRoles uvr, " +
            "org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk, org_hierarchy_service.users u where uvahk.userId= :userId and " +
            "uvahk.clientId= :clientId and uvr.clientId=uvahk.clientId and u.clientId=uvahk.clientId and uvahk.hierarchyKey=u.hierarchyKey and u.disabled=0 " +
            "and uvahk.hierarchyKey=uvr.hierarchyKey and uvr.role= :role ", nativeQuery = true)
    List<UserVsRoles> getUsersWithRole(@Param("userId") String userId, @Param("clientId") String clientId, @Param("role") String role);

    @Query(value = "SELECT uvr.userId, uvr.hierarchyKey, uvr.clientId, uvr.role FROM org_hierarchy_service.userVsRoles uvr, " +
            "org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk, org_hierarchy_service.users u where uvahk.userId= :userId and " +
            "uvahk.clientId= :clientId and uvr.clientId=uvahk.clientId and uvahk.hierarchyKey=uvr.hierarchyKey and uvr.role= :role " +
            "and  u.clientId=uvahk.clientId and uvahk.hierarchyKey=u.hierarchyKey and u.disabled=0",
            nativeQuery = true)
    List<UserVsRoles> getUsersHierarchyWithRole(@Param("userId") String userId, @Param("clientId") String clientId, @Param("role") String role);


    @Query(value = "SELECT uvr.userId, uvr.hierarchyKey, uvr.clientId, uvr.role FROM org_hierarchy_service.userVsRoles uvr, " +
            "org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk, org_hierarchy_service.users u where uvahk.userId= :userId and " +
            "uvahk.clientId= :clientId and uvr.clientId=uvahk.clientId and uvahk.hierarchyKey=uvr.hierarchyKey and uvr.role= :role " +
            "and u.clientId=uvahk.clientId and uvahk.hierarchyKey=u.hierarchyKey and u.disabled=0",
            nativeQuery = true)
    List<UserVsRoles> getDirectHierarchyWithRole(@Param("userId") String userId, @Param("clientId") String clientId, @Param("role") String role);

    @Query(value = "SELECT uvr.userId, uvr.hierarchyKey, uvr.clientId, uvr.role FROM org_hierarchy_service.userVsRoles uvr, " +
            "org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk where uvahk.userId= :userId and " +
            "uvahk.clientId= :clientId and uvr.clientId=uvahk.clientId and uvahk.hierarchyKey=uvr.hierarchyKey and uvr.role= :role and uvr.userId=:tmpUser",
            nativeQuery = true)
    UserVsRoles getNewUserUsersWithRole(@Param("userId") String userId, @Param("clientId") String clientId, @Param("role") String role, @Param("tmpUser") String tmpUser);
}
