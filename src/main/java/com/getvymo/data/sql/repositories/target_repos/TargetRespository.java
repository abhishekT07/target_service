package com.getvymo.data.sql.repositories.target_repos;

import java.util.List;

import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsAccessibleHierarchyKeys;
import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TargetRespository extends CrudRepository<UserVsTarget, Long> {

	public UserVsTarget findByUserCodeAndHierarchyKeyAndInstanceIdAndMetricFactId(String userCode, String hierarchyKey, int instanceId, int mfId);

	public UserVsTarget findByUserCodeAndInstanceIdAndMetricFactId(String userCode, int instanceId, int mfId);
	public List<UserVsTarget> findByUserCode(String userCode);

	public List<UserVsTarget> findByHierarchyKey(String list);

	UserVsTarget findByUserCodeAndInstanceId(String userCode, int instanceId);

	@Query(value = "select uvt.instance_id from targets_service.user_Vs_Target as uvt where  uvt.hierarchy_key in (:list)", nativeQuery = true)
	public List<UserVsTarget> findByHierarchyKeys(@Param("list") String s);

	@Modifying
	@Transactional
	@Query(value = "update targets_service.user_vs_target set value=:value where user_code=:userCode and metric_fact_id=:mfId and instance_id=:instanceId and hierarchy_key=:hKey", nativeQuery = true)
	public void updateTargetValue(@Param("value") double value, @Param("userCode") String userCode,
			  @Param("mfId") int mfId, @Param("instanceId") int instanceId, @Param("hKey") String hKey);

	@Modifying
	@Transactional
	@Query(value = "update targets_service.user_vs_target set value=:value, hierarchy_key=:hKey where user_code=:userCode and metric_fact_id=:mfId and instance_id=:instanceId", nativeQuery = true)
	public void updateTargetValueAndHKey(@Param("value") double value, @Param("userCode") String userCode,
								  @Param("mfId") int mfId, @Param("instanceId") int instanceId, @Param("hKey") String hKey);

	public void deleteByInstanceId(int instanceId);

	@Modifying
	@Transactional
	@Query(value = "update targets_service.user_vs_target set hierarchy_key= :hKey where user_code= :userCode and client_code= :clientCode", nativeQuery = true)
	public void updateHKeyOnRoleChange(@Param("userCode")String userCode, @Param("clientCode") String clientCode, @Param("hKey")String hKey);

	@Modifying
	@Transactional
	@Query(value = "update targets_service.user_vs_target uvt join org_hierarchy_service.users usr on " +
			"usr.userId=uvt.user_code join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on " +
			"uvahk.hierarchyKey=usr.hierarchyKey set uvt.hierarchy_key= usr.hierarchyKey where  uvahk.userId=:userCode " +
			"and uvahk.clientId=:clientCode and uvt.client_code=uvahk.clientId and usr.clientId=uvahk.clientId and uvt.date_key in (:date_keys)", nativeQuery = true)
	public void updateUVTOnManagerChange(@Param("userCode")String userCode, @Param("clientCode") String clientCode, @Param("date_keys")String date_keys);

	@Modifying
	@Transactional
	@Query(value = "update targets_service.user_vs_target set disabled= :disabled where user_code= :userCode and client_code= :clientCode and date_key in ( :date_keys )", nativeQuery = true)
	public void changeUserTargetVisibility(@Param("userCode") String userCode, @Param("clientCode") String clientCode, @Param("disabled") int disabled, @Param("date_keys") String date_keys);
}
