package com.getvymo.data.sql.repositories.target_repos;

import java.util.Date;
import java.util.List;

import com.getvymo.data.sql.entities.target_entities.DateKeys;
import com.getvymo.data.sql.entities.target_entities.DateKeysPK;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TargetRepositoryDate extends CrudRepository<DateKeys, DateKeysPK>{

	public List<DateKeys> findAll();
	public List<DateKeys> findAllByOrderByDateKeysPKAsc();
	
}
