package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.Facts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */

@Repository
public interface FactsRepository extends CrudRepository<Facts, String> {
//    public Facts findById(String factId);
    public List<Facts> findByClientCode(String clientCode);
}
