package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.TargetStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */
@Repository
public interface TargetStatusRepository extends CrudRepository<TargetStatus,String>{
    public List<TargetStatus> findByStatus(String status);
    public List<TargetStatus> findByUserVsTargetInstanceIdIn(List<Long> list);
    public  void deleteByUserVsTargetInstanceIdIn(List<Long> userVstargetInstanceId);
}
