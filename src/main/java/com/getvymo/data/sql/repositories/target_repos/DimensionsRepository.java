package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.Dimensions;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by abhishek on 31/03/17.
 */
public interface DimensionsRepository extends CrudRepository<Dimensions,Integer> {

    public List<Dimensions> findByClientCode(String clientCode);

    /**
     * Returns all instances of the type with the given IDs.
     *
     * @param integers
     * @return
     */
    @Override
    default Iterable<Dimensions> findAll(Iterable<Integer> integers) {
        return null;
    }
}
