package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by abhishek on 20/03/17.
 */

@Repository
public interface FactsMetricsRepository extends CrudRepository<FactMetrics,Integer> {
    public FactMetrics findByFactIdAndValue(String factId, String metricValue);
    public FactMetrics findByFactIdAndFactDimension(String factId, String metricValue);
    public ArrayList<FactMetrics> findById(int id);
}
