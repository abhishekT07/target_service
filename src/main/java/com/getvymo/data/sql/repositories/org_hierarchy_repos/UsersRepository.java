package com.getvymo.data.sql.repositories.org_hierarchy_repos;

import com.getvymo.data.sql.entities.org_hierarchy_entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by abhishek on 20/03/17.
 */


@Repository
public interface UsersRepository extends CrudRepository<Users,String >{
    public List<Users> getByHierarchyKey(String hKeys);
    public List<Users> getByUserIdAndClientIdAndDisabled(String userCode, String clientCode, int disabled);

    @Query(value = "SELECT  u.userId,  u.clientId,  u.hierarchyKey,  u.name,  u.email,  u.phone, u.params,  " +
            "u.isLatestKey,  u.disabled,  u.managerId,  u.disabledOn, u.updatedTime,  u.regionType FROM " +
            "org_hierarchy_service.users u,org_hierarchy_service.usersVsAccessibleHierarchyKeys acc where " +
            "u.hierarchyKey=acc.hierarchyKey and acc.clientId = :clientId and u.clientId= :clientId and acc.userId= :userId and u.disabled=0", nativeQuery = true)
    List<Users> getUsers(@Param("userId") String userId, @Param("clientId") String clientId);

    @Query(value = "SELECT  u.userId,  u.clientId,  u.hierarchyKey,  u.name,  u.email,  u.phone, u.params,  " +
            "u.isLatestKey,  u.disabled,  u.managerId,  u.disabledOn, u.updatedTime,  u.regionType FROM " +
            "org_hierarchy_service.users u,org_hierarchy_service.usersVsAccessibleHierarchyKeys acc where " +
            "u.hierarchyKey=acc.hierarchyKey and acc.clientId = :clientId and u.clientId= :clientId and acc.userId= :userId and u.userId= :tmpUser", nativeQuery = true)
    Users getNewUserForAllHierarchy(@Param("userId") String userId, @Param("clientId") String clientId, @Param("tmpUser")String tmpUser);

    @Query(value = "SELECT  u.userId,  u.clientId,  u.hierarchyKey,  u.name,  u.email,  u.phone, u.params,  " +
            "u.isLatestKey,  u.disabled,  u.managerId,  u.disabledOn, u.updatedTime,  u.regionType FROM " +
            "org_hierarchy_service.users u where " +
            "u.clientId = :clientId and u.managerId= :userId and u.disabled=0", nativeQuery = true)
    List<Users> getDirectHierarchy(@Param("userId") String userId, @Param("clientId") String clientId);

    @Query(value = "SELECT  u.userId,  u.clientId,  u.hierarchyKey,  u.name,  u.email,  u.phone, u.params,  " +
            "u.isLatestKey,  u.disabled,  u.managerId,  u.disabledOn, u.updatedTime,  u.regionType FROM " +
            "org_hierarchy_service.users u where " +
            "u.clientId = :clientId and u.managerId= :userId and u.userId=:tmpUser", nativeQuery = true)
    Users getNewUserForDirectHierarchy(@Param("userId") String userId, @Param("clientId") String clientId, @Param("tmpUser") String tmpUser);

    //Fecth userCode and hierarchyKeys for manager change queries

}
