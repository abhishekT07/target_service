package com.getvymo.data.sql.repositories.target_repos;

import com.getvymo.data.sql.entities.target_entities.TargetDefinition;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by abhishek on 20/03/17.
 */
public interface TargetDefinitionRepository extends CrudRepository<TargetDefinition, String> {
    public TargetDefinition findByFactId(String factId);
    public TargetDefinition findByFactIdAndClientCodeAndGranularity(String factId, String clientCode, String ganularity);
}
