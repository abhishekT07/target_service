package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Embeddable
public class DateKeysPK implements Serializable {

    @Column(name = "id", nullable = false)
    private Date id;

    @Column(name = "client_code")
    private String clientCode;

    public Date getId() {
        return id;
    }

    public void setId(Date id) {
        this.id = id;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public boolean equals(DateKeysPK instance){
        if(instance==null){
            return false;
        } else if(instance.getClientCode()==null){
            return false;
        } else if(instance.getId()==null){
            return false;
        }
        return instance.getClientCode().equals(clientCode) && instance.getId().equals(id);
    }

    public int hashCode(){
        return (id+clientCode).hashCode();
    }


}
