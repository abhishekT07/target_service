package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by abhishek on 31/03/17.
 */

@Entity
@Table(name = "dimensions", schema = "targets_service")
public class Dimensions {
    @Id
    private int id;
    private String dimensions;
    @Column(name = "client_code")
    private String clientCode;

    public Dimensions(int id, String dimensions, String clientCode) {
        this.id = id;
        this.dimensions = dimensions;
        this.clientCode = clientCode;
    }

    @Override
    public String toString() {
        return "Dimensions{" +
                "id=" + id +
                ", Dimensions='" + dimensions + '\'' +
                ", clientCode='" + clientCode + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public Dimensions() {
    }

    public ArrayList<String> getDimensionsList(){
        return (ArrayList<String>) Arrays.asList(this.dimensions.split(","));
    }
}
