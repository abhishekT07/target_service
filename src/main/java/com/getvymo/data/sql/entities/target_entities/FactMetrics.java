package com.getvymo.data.sql.entities.target_entities;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.persistence.*;

@Entity
@Table(name = "metric_fact")
public class FactMetrics {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "fact_id")
    private String factId;
    @Column(name = "fact_dimension")
    private String factDimension;
    private String value;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "unique_key")
    private String uniqueKey;
    @Column(name = "desc")
    private String desc;

    public FactMetrics() {

    }

    public FactMetrics(String factId, String factDimension, String value, String clientCode, String uniqueKey, String desc) {
        this.factId = factId;
        this.factDimension = factDimension;
        this.value = value;
        this.clientCode = clientCode;
        this.uniqueKey = uniqueKey;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFactId() {
        return factId;
    }

    public void setFactId(String factId) {
        this.factId = factId;
    }

    public String getFactDimension() {
        return factDimension;
    }

    public void setFactDimension(String factDimension) {
        this.factDimension = factDimension;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }
}
