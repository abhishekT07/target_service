package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by abhishek on 20/03/17.
 */
@Entity
@Table(name = "target_status", schema = "targets_service")
public class TargetStatus {
    @Id
    @Column(name = "user_vs_target_instance_id")
    private Long userVsTargetInstanceId;
    @Column(name = "job_id")
    private String jobId;
    private String status;

    public TargetStatus(Long userVsTargetInstanceId, String jobId, String status) {
        this.userVsTargetInstanceId = userVsTargetInstanceId;
        this.jobId = jobId;
        this.status = status;
    }

    public TargetStatus() {
    }

    public Long getUserVsTargetInstanceId() {
        return userVsTargetInstanceId;
    }

    public void setUserVsTargetInstanceId(Long userVsTargetInstanceId) {
        this.userVsTargetInstanceId = userVsTargetInstanceId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
