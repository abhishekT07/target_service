package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.*;

/**
 * Created by abhishek on 20/03/17.
 */
@Entity
@Table(name = "user_vs_target_instance", schema = "targets_service")
public class UserVsTargetInstance  {

    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "user_code")
    private String userCode;
    @Column(name = "role")
    private String role;
    @Column(name = "target_instance_id")
    private int targetInstanceId;
    @Column(name = "date_key")
    private String dateKey;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "target_value")
    private Double targetValue;
    @Column(name = "assignment_type")
    private String assignmentType;

    public UserVsTargetInstance() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserVsTargetInstance(long id, String userCode, String role, int targetInstanceId, String dateKey, String clientCode, Double targetValue, String assignmentType) {
        this.id = id;
        this.userCode = userCode;
        this.role = role;
        this.targetInstanceId = targetInstanceId;
        this.dateKey = dateKey;
        this.clientCode = clientCode;
        this.targetValue = targetValue;
        this.assignmentType = assignmentType;
    }

    public String getAssignmentType() {
        return assignmentType;
    }

    public void setAssignmentType(String assignmentType) {
        this.assignmentType = assignmentType;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getTargetInstanceId() {
        return targetInstanceId;
    }

    public void setTargetInstanceId(int targetInstanceId) {
        this.targetInstanceId = targetInstanceId;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public Double getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(Double targetValue) {
        this.targetValue = targetValue;
    }

    @Override
    public String toString() {
        return "UserVsTargetInstance{" +
                "id=" + id +
                ", userCode='" + userCode + '\'' +
                ", role='" + role + '\'' +
                ", targetInstanceId=" + targetInstanceId +
                ", dateKey='" + dateKey + '\'' +
                ", clientCode='" + clientCode + '\'' +
                ", targetValue=" + targetValue +
                '}';
    }
}
