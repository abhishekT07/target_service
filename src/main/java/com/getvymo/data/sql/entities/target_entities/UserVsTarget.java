package com.getvymo.data.sql.entities.target_entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "user_vs_target", schema = "targets_service")
@IdClass(UserVsTarget.class)
public class UserVsTarget implements Serializable {
    //TODO(ABHISHEK): see if this actually needed
    static final long serialVersionUID = 42L;
    @Id
    @Column(name = "user_code")
    private String userCode;
    @Column(name = "hierarchy_key")
    private String hierarchyKey;
    @Id
    @Column(name = "instance_id")
    private Integer instanceId;
    @Id
    @Column(name = "metric_fact_id")
    private int metricFactId;
    @Column(name = "date_key")
    private String dateKey;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "value")
    private double value;
    @Column(name = "disabled")
    private int disabled;

    public UserVsTarget() {
    }

    public UserVsTarget(String userCode, String hierarchyKey, Integer instanceId, int metricFactId, String dateKey, String clientCode, double value, int disabled) {
        this.userCode = userCode;
        this.hierarchyKey = hierarchyKey;
        this.instanceId = instanceId;
        this.metricFactId = metricFactId;
        this.dateKey = dateKey;
        this.clientCode = clientCode;
        this.value = value;
        this.disabled = disabled;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public int getMetricFactId() {
        return metricFactId;
    }

    public void setMetricFactId(int metricFactId) {
        this.metricFactId = metricFactId;
    }

    public String getDateKey() {
        return dateKey;
    }


    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserVsTarget that = (UserVsTarget) o;

        if (metricFactId != that.metricFactId) return false;
        if (!userCode.equals(that.userCode)) return false;
        if (!hierarchyKey.equals(that.hierarchyKey)) return false;
        return instanceId.equals(that.instanceId);
    }

    @Override
    public int hashCode() {
        int result = userCode.hashCode();
        result = 31 * result + hierarchyKey.hashCode();
        result = 31 * result + instanceId.hashCode();
        result = 31 * result + metricFactId;
        return result;
    }
}
