package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.*;

@Entity
@Table(name = "facts", schema = "targets_service")
public class Facts {

    @Id
    private String id;
    private String metric;
    private String module;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "state")
    private String state;
    @Column(name = "dimensionId")
    private String dimensionId;
    @Column(name = "target_type")
    private String targetType;
    @Column(name = "desc")
    private String desc;
    @Column(name = "id_metadata")
    private String metadata;

    public Facts() {
    }

    public Facts(String id, String metric, String module, String clientCode, String state, String dimensionId, String targetType, String desc, String metadata) {
        this.id = id;
        this.metric = metric;
        this.module = module;
        this.clientCode = clientCode;
        this.state = state;
        this.dimensionId = dimensionId;
        this.targetType = targetType;
        this.desc = desc;
        this.metadata = metadata;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(String dimensionId) {
        this.dimensionId = dimensionId;
    }
}
