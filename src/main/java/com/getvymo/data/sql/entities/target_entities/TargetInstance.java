package com.getvymo.data.sql.entities.target_entities;

import javax.persistence.*;

@Entity
@Table(name = "target_instance", schema = "targets_service")
public class TargetInstance {
    @Id
    private int id;
    @Column(name = "def_id")
    private int defId;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "metric_fact_id")
    private int metricFactId;
    @Column(name = "date_key")
    private String dateKey;
    @Column(name = "achievement_type")
    private String achievementType;
    @Column(name = "label")
    private String label;
    @Column(name = "description")
    private String description;

    public TargetInstance() {
    }

    public TargetInstance(int id, int defId, String clientCode, int metricFactId,
                          String dateKey, String achievementType, String label, String description) {
        this.id = id;
        this.defId = defId;
        this.clientCode = clientCode;
        this.metricFactId = metricFactId;
        this.dateKey = dateKey;
        this.achievementType = achievementType;
        this.label = label;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDefId() {
        return defId;
    }

    public void setDefId(int defId) {
        this.defId = defId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public int getMetricFactId() {
        return metricFactId;
    }

    public void setMetricFactId(int metricFactId) {
        this.metricFactId = metricFactId;
    }

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getAchievementType() {
        return achievementType;
    }

    public void setAchievementType(String achievementType) {
        this.achievementType = achievementType;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "TargetInstance{" +
                "id=" + id +
                ", defId=" + defId +
                ", clientCode='" + clientCode + '\'' +
                ", metricFactId=" + metricFactId +
                ", dateKey='" + dateKey + '\'' +
                ", achievementType='" + achievementType + '\'' +
                ", label='" + label + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
