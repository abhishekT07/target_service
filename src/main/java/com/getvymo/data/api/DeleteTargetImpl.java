package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.entities.target_entities.TargetStatus;
import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
import com.getvymo.data.sql.entities.target_entities.UserVsTargetInstance;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetRespository;
import com.getvymo.data.sql.repositories.target_repos.TargetStatusRepository;
import com.getvymo.data.sql.repositories.target_repos.UserVsTargetInstanceRepository;
import com.getvymo.exceptions.ErrorException;
import com.getvymo.exceptions.ExcelRowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhishek on 20/04/17.
 */
@Controller
public class DeleteTargetImpl implements IDeletetarget {
    @Autowired
    TargetInstanceRepository targetInstanceRepository;
    @Autowired
    TargetRespository targetRespository;
    @Autowired
    UserVsTargetInstanceRepository userVsTargetInstanceRepository;
    @Autowired
    TargetStatusRepository targetStatusRepository;

    @Override
    public TargetInstance isClientTarget(int instanceId, String clientCode) throws ErrorException {
        TargetInstance targetInstance = targetInstanceRepository.findById(instanceId);
        if (targetInstance!=null){
            if (targetInstance.getClientCode().equals(clientCode)){
                return targetInstance;
            } else {
                throw new ErrorException("This target for client "+clientCode+" does not exist");
            }
        } else
            throw new ErrorException("This target for client "+clientCode+" does not exist");
    }

    @Override
    @Transactional(transactionManager = "dsTargetsTransactionManager")
    public boolean deleteTarget(TargetInstance targetInstance) throws ErrorException {
        int instanceId = targetInstance.getId();
        List<UserVsTargetInstance> userVsTargetInstanceList = userVsTargetInstanceRepository.findByTargetInstanceId(instanceId);
        List<Long> userVsTargetInstanceIdList = new ArrayList<Long>();
        for (UserVsTargetInstance userVsTargetInstance: userVsTargetInstanceList){
            userVsTargetInstanceIdList.add(userVsTargetInstance.getId());
        }
        List<TargetStatus> targetStatusList = targetStatusRepository.findByUserVsTargetInstanceIdIn(userVsTargetInstanceIdList);
        targetStatusRepository.delete(targetStatusList);
        targetRespository.deleteByInstanceId(instanceId);
        userVsTargetInstanceRepository.delete(userVsTargetInstanceList);
        targetInstanceRepository.delete(targetInstance);
        return true;
    }
}
