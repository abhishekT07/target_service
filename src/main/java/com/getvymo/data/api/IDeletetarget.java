package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.exceptions.ErrorException;

/**
 * Created by abhishek on 20/04/17.
 */
public interface IDeletetarget {
    public TargetInstance isClientTarget(int instanceId, String clientCode) throws ErrorException;

    public boolean deleteTarget(TargetInstance instanceId) throws ErrorException;
}
