package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.entities.target_entities.TargetStatus;
import com.getvymo.data.sql.entities.target_entities.UserVsTargetInstance;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetStatusRepository;
import com.getvymo.data.sql.repositories.target_repos.UserVsTargetInstanceRepository;
import com.getvymo.importer.RowStatus;
import com.getvymo.importer.TargetImportConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by abhishek on 06/04/17.
 */
public class PersistTargetImpl implements PersistTarget {
    private static Logger logger = LoggerFactory.getLogger(PersistTargetImpl.class);
    @Override
    public RowStatus createIndividualInstances(HashMap<String, String> metaData, HashMap<String, String> info, String clientCode, TargetInstanceRepository targetInstanceRepository, UserVsTargetInstanceRepository userVsTargetInstanceRepository, TargetStatusRepository targetStatusRepository) {
        TargetInstance newTargetInstance=null;
        logger.info("TargetMetadata and target info");
        try {
             newTargetInstance= targetInstanceRepository.findByClientCodeAndMetricFactIdAndDateKeyAndAchievementType(
                    clientCode,(int)Double.parseDouble(metaData.get("metric_fact_id")),
                    metaData.get("date_key"),info.get("achievement_type"));
             logger.info("after find target instance query"+newTargetInstance+clientCode+metaData.get("metric_fact_id")+info.get("assignment_type")+metaData.get("date_key")+info.get("achievement_type"));
            if (newTargetInstance != null){
                logger.info("Target instance already exists" + newTargetInstance.toString());
                // proceed with UserVsTargetInstanceCreation
                // Somebody Might want to change the label or description
                logger.info("Target Label and desc"+info.get("label")+"--"+info.get("description"));
                newTargetInstance.setLabel(info.get("label")+" ("+metaData.get("granularity")+")");
                newTargetInstance.setDescription(info.get("description"));
                targetInstanceRepository.save(newTargetInstance);
            } else {
                //create target instance now
                logger.info("Target instance not found, creating one");
                int randomNum = ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
                System.out.println("info"+info.toString()+metaData.toString());
                TargetInstance targetInstance = new TargetInstance(randomNum, (int)Double.parseDouble(metaData.get("def_id")), clientCode,
                        (int)Double.parseDouble(metaData.get("metric_fact_id")), metaData.get("date_key"),
                        info.get("achievement_type"), info.get("label") + " (" +
                        metaData.get("granularity") + ")", info.get("description"));

                newTargetInstance = targetInstanceRepository.save(targetInstance);
            }

            RowStatus currentRowStatus = createUserVsTargetInstance(info.get("userCode"),
                    info.get("role"), newTargetInstance.getId(), metaData.get("date_key"), clientCode,
                    Double.parseDouble(info.get("target_value")),info.get("assignment_type"),userVsTargetInstanceRepository, targetStatusRepository);

            return currentRowStatus;
        } catch (Exception e){
            logger.error("CRITICAL: failed to create target instance"+e.getMessage());
            e.printStackTrace();
            return new RowStatus(false,"Failed to create Target, try again later");
        }

    }

    public RowStatus createUserVsTargetInstance(String userCode, String role, int targetInstanceId, String
            date_key, String clientCode, Double target_value, String assignmentType, UserVsTargetInstanceRepository userVsTargetInstanceRepository,
                                                TargetStatusRepository targetStatusRepository){
        if(role==null){
            logger.info("NO ROLE FOUND"+role);
            role="NA";
        }
        Long randomLong = ThreadLocalRandom.current().nextLong(Long.MIN_VALUE, Long.MAX_VALUE);
        UserVsTargetInstance userVsTargetInstance=null;
        try {
            userVsTargetInstance = new UserVsTargetInstance(randomLong, userCode, role, targetInstanceId, date_key,
                    clientCode, target_value, assignmentType);
            UserVsTargetInstance newuserVsTargetInstance = userVsTargetInstanceRepository.save(userVsTargetInstance);
            TargetStatus newTargetStatus = createTargetStatus(newuserVsTargetInstance.getId(),targetStatusRepository);
            return new RowStatus(true,"Successful");
        } catch (DataIntegrityViolationException di) {
            //This means instance already exists so need to update it
            userVsTargetInstance = userVsTargetInstanceRepository.findByUserCodeAndRoleAndDateKeyAndClientCodeAndTargetInstanceId(
                    userCode, role, date_key, clientCode, targetInstanceId);
            userVsTargetInstance.setTargetValue(target_value);
            userVsTargetInstanceRepository.save(userVsTargetInstance);
            TargetStatus newTargetStatus = createTargetStatus(userVsTargetInstance.getId(),targetStatusRepository);
            logger.warn("Duplicate userVsTargetInstance creation for"+userVsTargetInstance.toString());
            return new RowStatus(true,"Target updated as it already exists");
        } catch (Exception e) {
            logger.warn("Failed to create userVsTargetInstance"+e.getMessage());
            e.printStackTrace();
            return new RowStatus(false,"Target creation failed");
        }
    }

    public TargetStatus createTargetStatus(Long userVsTargetInsatnceId, TargetStatusRepository targetStatusRepository){
        // We have to trigger batch job from here
        String jobId = UUID.randomUUID().toString();
        try {
            TargetStatus targetStatus = new TargetStatus(userVsTargetInsatnceId, jobId, "INCOMPLETE");
            return targetStatusRepository.save(targetStatus);
        } catch (Exception e){
            logger.error("CRITICAL: Failed to create target status record"+e.getMessage());
            return null;
        }

    }
}
