package com.getvymo.data.api;

import com.getvymo.cache.fact_metrics.IFactsMetricsCache;
import com.getvymo.cache.facts.IFactsCache;
import com.getvymo.data.sql.entities.target_entities.FactMetrics;
import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 21/04/17.
 */
@Controller
public class TargetMgmntImpl implements ITargetMgmnt {
    Logger logger = LoggerFactory.getLogger(TargetMgmntImpl.class);
    @Autowired
    TargetInstanceRepository targetInstanceRepository;
    @Autowired
    FactsMetricsRepository factsMetricsRepository;
    @Autowired
    FactsRepository factsRepository;
    @Autowired
    IFactsMetricsCache iFactsMetricsCache;
    @Autowired
    IFactsCache iFactsCache;

    @Override
    public List<TargetInstance> fetchClientTargets(String clientCode) throws Exception{

        return targetInstanceRepository.findByClientCode(clientCode);
    }

    @Override
    public HashMap<Integer, String> fetchClientMetricFacts(String clientCode) {
        ArrayList<FactMetrics> factMetricss = iFactsMetricsCache.getAllMetricFactsByClient(clientCode);
        HashMap<Integer,String> targetMapping = new HashMap<>();
        for (FactMetrics factMetrics: factMetricss){
            String factMetricDesc = factMetrics.getDesc();
            String factDesc = iFactsCache.getTargetFactDesc(factMetrics.getFactId());
            String finalDesc = (factMetricDesc != null && factMetricDesc.length() > 0) ? factDesc + " for " + factMetricDesc : factDesc;
            targetMapping.put(factMetrics.getId(),finalDesc);
        }
        return targetMapping;
    }
}
