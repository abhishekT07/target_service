package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetStatusRepository;
import com.getvymo.data.sql.repositories.target_repos.UserVsTargetInstanceRepository;
import com.getvymo.importer.RowStatus;

import java.util.HashMap;

/**
 * Created by abhishek on 06/04/17.
 */
public interface PersistTarget {
    public RowStatus createIndividualInstances(HashMap<String, String> metaData, HashMap<String,String> info,
                                               String clientCode, TargetInstanceRepository targetInstanceRepository, UserVsTargetInstanceRepository
                                        userVsTargetInstanceRepository, TargetStatusRepository targetStatusRepository);
}
