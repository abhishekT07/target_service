package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetDefinition;
import com.getvymo.data.sql.repositories.target_repos.TargetDefinitionRepository;

/**
 * Created by abhishek on 06/04/17.
 */
public interface PersistTargetDefinition {
    public TargetDefinition createTargetDefinition(String granularity, String factId, String clientCode,
                                                   TargetDefinitionRepository targetDefinitionRepository) throws Exception;
}
