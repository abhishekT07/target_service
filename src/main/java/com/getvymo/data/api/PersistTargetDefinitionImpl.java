package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetDefinition;
import com.getvymo.data.sql.repositories.target_repos.TargetDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by abhishek on 06/04/17.
 */
public class PersistTargetDefinitionImpl implements PersistTargetDefinition {
    private static Logger logger = LoggerFactory.getLogger(PersistTargetDefinitionImpl.class);

    @Override
    public TargetDefinition createTargetDefinition( String granularity, String factId, String clientCode,
                                                   TargetDefinitionRepository targetDefinitionRepository) throws Exception {
        try {
            TargetDefinition newDef = targetDefinitionRepository.findByFactIdAndClientCodeAndGranularity(factId,
                    clientCode, granularity);
            if (newDef != null) {
                logger.info("Target definition already exists " + newDef.toString());
                return newDef;
            } else {
                logger.info("Target definition not found, creating new one");
                int randomNum = ThreadLocalRandom.current().nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
                newDef = new TargetDefinition(randomNum, granularity,
                        factId, clientCode);

                TargetDefinition newOne = targetDefinitionRepository.save(newDef);
                return newDef;
            }
        } catch (Exception e) {
            logger.info("Failed to Create target definition" + factId + clientCode + granularity + e.getMessage());
            e.printStackTrace();
            throw e;
        }
    }
}
