package com.getvymo.data.api;

import com.getvymo.data.sql.entities.target_entities.TargetInstance;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 21/04/17.
 */
public interface ITargetMgmnt {
    public List<TargetInstance> fetchClientTargets(String clientCode) throws Exception;

    public HashMap fetchClientMetricFacts(String clientCode);
}
