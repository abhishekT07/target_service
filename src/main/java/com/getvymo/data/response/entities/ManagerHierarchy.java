//package com.getvymo.data.response.entities;
//
//import com.fasterxml.jackson.annotation.JsonRootName;
//import com.fasterxml.jackson.annotation.*;
//import java.io.Serializable;
//import java.util.List;
//
//public class ManagerHierarchy extends ActiveTargets implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	@JsonProperty("client_id")
//	private String clientId;
//	@JsonProperty("hierarchy_id")
//	private String hierarchyId;
//
//	public String getClientId() {
//		return clientId;
//	}
//
//	public void setClientId(String clientId) {
//		this.clientId = clientId;
//	}
//
//	public String getHierarchyId() {
//		return hierarchyId;
//	}
//
//	public void setHierarchyId(String hierarchyId) {
//		this.hierarchyId = hierarchyId;
//	}
//
//	public ManagerHierarchy(String name, int instanceId, String factDefId, int metricId, String metricFactId,
//			String metricValue, int value, String teamCardinality, String clientId, String hierarchyId) {
//		super(name, instanceId, factDefId, metricId, metricFactId, metricValue, value, teamCardinality);
//
//		this.clientId = clientId;
//		this.hierarchyId = hierarchyId;
//
//	}
//
//}
