package com.getvymo.data.response.entities;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.*;
import java.io.Serializable;
import java.sql.Date;
public class MetricFacts implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String factDimension;
	private String value;
	
	@JsonProperty("fact_dimension")
	public String getFactDimension() {
		return factDimension;
	}
	@JsonProperty("fact_dimension")
	public void setFactDimension(String factDimension) {
		this.factDimension = factDimension;
	}
	@JsonProperty("value")
	public String getValue() {
		return value;
	}
	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}
	

}
