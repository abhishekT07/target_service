package com.getvymo.data.response.entities;

public class EmailSummary {
	
	int total;
	String metricFactId;
	String userId;
	int instanceId;
	public int getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getMetricFactId() {
		return metricFactId;
	}
	public void setMetricFactId(String metricFactId) {
		this.metricFactId = metricFactId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	

}
