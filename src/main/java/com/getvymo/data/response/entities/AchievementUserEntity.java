package com.getvymo.data.response.entities;

/**
 * Created by abhishek on 12/04/17.
 */
public class AchievementUserEntity {
    private String user_code;

    public String getUser_code() {
        return user_code;
    }

    public void setUser_code(String user_code) {
        this.user_code = user_code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AchievementUserEntity that = (AchievementUserEntity) o;

        return user_code != null ? user_code.equals(that.user_code) : that.user_code == null;
    }

    @Override
    public int hashCode() {
        return user_code != null ? user_code.hashCode() : 0;
    }
}
