package com.getvymo.data.response.entities;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LevelwiseSummary implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String  userId;
	
	private Integer targetValue;
	private Integer instanceId;
	List<LevelwiseSummary> mappingResult;
	
	private ActiveTargets activeTargets;
	@JsonProperty("targets")
	public ActiveTargets getActiveTargets() {
		return activeTargets;
	}
	@JsonProperty("targets")
	public void setActiveTargets(ActiveTargets activeTargets) {
		this.activeTargets = activeTargets;
	}
	public List<LevelwiseSummary> getMappingResult() {
		return mappingResult;
	}
	public void setMappingResult(List<LevelwiseSummary> mappingResult) {
		this.mappingResult = mappingResult;
	}
	//	LevelwiseSummary mappingResult;
//	
//	
//	public LevelwiseSummary getMappingResult() {
//		return mappingResult;
//	}
//	public void setMappingResult(LevelwiseSummary mappingResult) {
//		this.mappingResult = mappingResult;
//	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(Integer instanceId) {
		this.instanceId = instanceId;
	}
	public Integer getTargetValue() {
		return targetValue;
	}
	public void setTargetValue(Integer targetValue) {
		this.targetValue = targetValue;
	}
	@JsonProperty("user")
	public String getUserId() {
		return userId;
	}
	@JsonProperty("user")
	public void setUserId(String userId) {
		this.userId = userId;
	}
	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			LevelwiseSummary summary = (LevelwiseSummary) object;
			if ( this.instanceId.equals(summary.instanceId)&&this.targetValue.equals(summary.targetValue)) {
				result = true;
			}
		}
		return result;
	}

	// just omitted null checks
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 7 * hash + this.targetValue.hashCode();
		hash = 7 * hash + this.instanceId.hashCode();
		return hash;
	}
	

}
