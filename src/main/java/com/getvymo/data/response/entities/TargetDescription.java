package com.getvymo.data.response.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by abhishek on 21/04/17.
 */
public class TargetDescription {
    @JsonProperty(value = "target_label")
    private String targetLabel;
    @JsonProperty(value = "target_dimensions")
    private String targetDimensions;
    @JsonProperty(value = "start_date")
    private long start_date;
    @JsonProperty(value = "end_date")
    private long end_date;

    public TargetDescription(String targetLabel, String targetDimensions, long start_date, long end_date) {
        this.targetLabel = targetLabel;
        this.targetDimensions = targetDimensions;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public TargetDescription() {
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public String getTargetLabel() {
        return targetLabel;
    }

    public void setTargetLabel(String targetLabel) {
        this.targetLabel = targetLabel;
    }

    public String getTargetDimensions() {
        return targetDimensions;
    }

    public void setTargetDimensions(String targetDimensions) {
        this.targetDimensions = targetDimensions;
    }
}
