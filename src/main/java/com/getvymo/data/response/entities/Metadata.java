package com.getvymo.data.response.entities;

/**
 * Created by abhishek on 17/04/17.
 */
public class Metadata {
    private String type;
    private String metric_type;
    private String value;

    public Metadata() {
    }

    public Metadata(String type, String metric_type, String value) {
        this.type = type;
        this.metric_type = metric_type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMetric_type() {
        return metric_type;
    }

    public void setMetric_type(String metric_type) {
        this.metric_type = metric_type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
