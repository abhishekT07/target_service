package com.getvymo.achievement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getvymo.achievement.metric_rest_pojos.*;
import com.getvymo.cache.facts.IFactsCache;
import com.getvymo.cache.facts.FactsCacheImpl;
import com.getvymo.data.response.entities.AchievementUserEntity;
import com.getvymo.targets.data.api.MetricFactParser;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by abhishek on 25/03/17.
 */

@PropertySource("classpath:application.properties")
public class GetAchievementImpl implements GetAchievement {
    Logger logger = LoggerFactory.getLogger(GetAchievementImpl.class);

    private static final String METRICS_REST_URL = "local.metrics.rest.url";
    @Resource
    private Environment env;
//    @Autowired
//    private FactsMetricsRepository factsMetricsRepository;
    @Override
    public JSONObject getAchievement(int metric_fact_id, String start, String end, String
            clientCode, List<AchievementUserEntity> achievementUserEntityList) {
        MetricFactParser factParser = new MetricFactParser();
        String fact_id = factParser.getFactId(metric_fact_id);
        String factValue = (factParser.getFactValue(fact_id));
        String factDimensions = (factParser.getFactDimensions(metric_fact_id));
        IFactsCache iFactsCache = new FactsCacheImpl();
        String module= iFactsCache.getFactModule(fact_id);
        ArrayList<String> userIdList = new ArrayList<String>();
        String factMetric = iFactsCache.getFactMetric(fact_id);
        if (achievementUserEntityList!=null && !achievementUserEntityList.isEmpty()){
            for (AchievementUserEntity userEntity:achievementUserEntityList){
                userIdList.add(userEntity.getUser_code());
            }
        }
        AchievementBody body = getRequestPayload(factValue, null,null, start, end, clientCode, factDimensions,
                module, userIdList.toArray(new String[userIdList.size()]), factMetric,"userId");
        Map<String,Object> responseMap = fetchHttpResponse(body);
        Gson gson = new Gson();
        String jsonString = gson.toJson(responseMap);
        JSONObject jsonObject = (JSONObject) JSONValue.parse(jsonString);//new JSONObject(jsonString);
        return jsonObject;
    }

    @Override
    public float getAchievement(int metric_fact_id, String userCode, String self, String start, String end, String
            clientCode, String fact_id, List<AchievementUserEntity> achievementUserEntityList) {
        logger.info("Received achievemnt call for mf_id,user,self,start,end,client,fact_id,userids-->>" + metric_fact_id
            + "," + userCode + "," + self + "," + start + "," + end + "," + clientCode + "," + fact_id + "," + achievementUserEntityList);
        MetricFactParser factParser = new MetricFactParser();
        String factValue = (factParser.getFactValue(fact_id));
        String factDimensions = (factParser.getFactDimensions(metric_fact_id));
        IFactsCache iFactsCache = new FactsCacheImpl();
//        String module= "activities";
        String module= iFactsCache.getFactModule(fact_id);
        ArrayList<String> userIdList = new ArrayList<String>();
        String factMetric = iFactsCache.getFactMetric(fact_id);
        if (achievementUserEntityList!=null && !achievementUserEntityList.isEmpty()){
            for (AchievementUserEntity userEntity:achievementUserEntityList){
                userIdList.add(userEntity.getUser_code());
            }
        }
        AchievementBody body = getRequestPayload(factValue, userCode, self, start, end, clientCode, factDimensions,
                module, userIdList.toArray(new String[userIdList.size()]), factMetric,null);

        Map<String,Object> response = fetchHttpResponse(body);
        float achievement  = 0;
        if(response!=null){
            achievement = Float.parseFloat(response.get("value")+"");
            return achievement;
        } else
            return 0;

    }

    private static AchievementBody getRequestPayload(String factValue, String userCode, String self, String start,
                                                     String end, String clientCode, String factDimension, String
                                                             module, String[] userIds,String factMetric, String groupParam){
        //Build filters individually
        Filters[] filters = new Filters[3];
        // Dimension Filter
        filters[0] = new DimCombFilter(module,factMetric,factValue,"dimensionCombination",factDimension);

        //User Filter
        IUserFilterValue iUserFilterValue = null;
        UserFilterDef userFilterDef = null;
        if (userIds != null && userIds.length > 0) {
            iUserFilterValue = new UserFilterValueList(userIds, "");
            userFilterDef = new UserFilterDef("list", "team");
        } else if (self.equals("true")) {
            String[] user = {userCode};
            iUserFilterValue = new UserFilterValueList(user, "");
            userFilterDef = new UserFilterDef("list", "self");
        } else if (self.equals("false")) {
            String[] user = {userCode};
            iUserFilterValue = new UserFilterValueList(user, "");
            userFilterDef = new UserFilterDef("list", "team");
        }

        filters[1] =  new UserFilters(userFilterDef,
                iUserFilterValue, "userId");

        // Date filter
        filters[2] = new DateFilter(new DateFilterValue(start,end),"date");

        String[] entitities = {"achievements"};
        String[] groupBy = new String[1];
        if (groupParam != null) {
            groupBy[0] = groupParam;
        } else {
            String[] tmp = {};
            groupBy=tmp;
        }
        Params params= new Params(groupBy,entitities,filters, clientCode );
        AchievementBody body = new AchievementBody(params);

        return body;
    }

    private  Map<String,Object> fetchHttpResponse(AchievementBody achievementBody){
        String restUrl =  "http://localhost:3003/v1/metrics/query/";
        System.out.println("Achievement URL" + restUrl);
        Map<String, Object> jsonMap= null;
        CloseableHttpClient httpClient    = HttpClientBuilder.create().build();
        HttpPost post          = new HttpPost(restUrl);
        Gson gson= new Gson();
        CloseableHttpResponse response=null;
        try {
            StringEntity postingString = new StringEntity(gson.toJson(achievementBody));
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            response = httpClient.execute(post);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() >= 300) {
                throw new HttpResponseException(
                        statusLine.getStatusCode(),
                        statusLine.getReasonPhrase());
            }
            HttpEntity entity = response.getEntity();
            if (entity == null) {
                throw new ClientProtocolException("Response contains no content");
            }
            InputStream inputStream = entity.getContent();
            ObjectMapper mapper = new ObjectMapper();
            jsonMap = mapper.readValue(inputStream, Map.class);
            response.close();
        } catch (IOException exception) {
            System.out.println("Failed to make achievement call" + exception);
            exception.printStackTrace();
            return null;
        }

        return jsonMap;

    }
}
