package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class UserFilterDef {
    private String userFilterType;

    private String userHierarchyType;

    public UserFilterDef() {
    }

    public UserFilterDef(String userFilterType, String userHierarchyType) {
        this.userFilterType = userFilterType;
        this.userHierarchyType = userHierarchyType;
    }

    public String getUserFilterType() {
        return userFilterType;
    }

    public void setUserFilterType(String userFilterType) {
        this.userFilterType = userFilterType;
    }

    public String getUserHierarchyType() {
        return userHierarchyType;
    }

    public void setUserHierarchyType(String userHierarchyType) {
        this.userHierarchyType = userHierarchyType;
    }

    @Override
    public String toString() {
        return "ClassPojo [userFilterType = " + userFilterType + ", userHierarchyType = " + userHierarchyType + "]";
    }
}
