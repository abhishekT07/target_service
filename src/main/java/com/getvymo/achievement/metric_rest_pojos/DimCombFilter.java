package com.getvymo.achievement.metric_rest_pojos;

import com.google.gson.JsonObject;

/**
 * Created by abhishek on 25/03/17.
 */
public class DimCombFilter implements Filters {
    private String module;

    private String metric;
    private String value;

    private String filterEntityId;

    private String dimensionIdMap;

    public DimCombFilter() {
    }

    public DimCombFilter(String module, String metric, String value, String filterEntityId, String dimensionIdMap) {
        this.module = module;
        this.metric = metric;
        this.value = value;
        this.filterEntityId = filterEntityId;
        this.dimensionIdMap = dimensionIdMap;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFilterEntityId() {
        return filterEntityId;
    }

    public void setFilterEntityId(String filterEntityId) {
        this.filterEntityId = filterEntityId;
    }

    public String getDimensionIdMap() {
        return dimensionIdMap;
    }

    public void setDimensionIdMap(String dimensionIdMap) {
        this.dimensionIdMap = dimensionIdMap;
    }

    @Override
    public String toString() {
        return "ClassPojo [module = " + module + ", value = " + value + ", filterEntityId = " + filterEntityId + ", dimensionIdMap = " + dimensionIdMap + "]";
    }
}
