package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class DateFilter implements Filters {

    private DateFilterValue value;

    private String filterEntityId;

    public DateFilter() {
    }

    public DateFilter(DateFilterValue value, String filterEntityId) {
        this.value = value;
        this.filterEntityId = filterEntityId;
    }
}
