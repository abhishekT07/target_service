package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class DimensionValue {
    private String actualLastUpdateStateType;

    public DimensionValue(String actualLastUpdateStateType) {
        this.actualLastUpdateStateType = actualLastUpdateStateType;
    }

    public DimensionValue() {
    }

    public String getActualLastUpdateStateType() {
        return actualLastUpdateStateType;
    }

    public void setActualLastUpdateStateType(String actualLastUpdateStateType) {
        this.actualLastUpdateStateType = actualLastUpdateStateType;
    }

    @Override
    public String toString() {
        return "ClassPojo [actualLastUpdateStateType = " + actualLastUpdateStateType + "]";
    }
}
