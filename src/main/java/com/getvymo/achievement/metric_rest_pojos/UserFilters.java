package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class UserFilters implements Filters {
    private UserFilterDef userFilterDef;

    private IUserFilterValue value;

    private String filterEntityId;

    public UserFilters() {
    }

    public UserFilters(UserFilterDef userFilterDef, IUserFilterValue value, String filterEntityId) {
        this.userFilterDef = userFilterDef;
        this.value = value;
        this.filterEntityId = filterEntityId;
    }

    public UserFilterDef getUserFilterDef() {
        return userFilterDef;
    }

    public void setUserFilterDef(UserFilterDef userFilterDef) {
        this.userFilterDef = userFilterDef;
    }

    public IUserFilterValue getValue() {
        return value;
    }

    public void setValue(IUserFilterValue value) {
        this.value = value;
    }

    public String getFilterEntityId() {
        return filterEntityId;
    }

    public void setFilterEntityId(String filterEntityId) {
        this.filterEntityId = filterEntityId;
    }

    @Override
    public String toString() {
        return "ClassPojo [userFilterDef = " + userFilterDef + ", value = " + value + ", filterEntityId = " + filterEntityId + "]";
    }
}
