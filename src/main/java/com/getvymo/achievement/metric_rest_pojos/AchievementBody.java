package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class AchievementBody {
    private Params params;

    public Params getParams() {
        return params;
    }

    public AchievementBody(Params params) {
        this.params = params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "ClassPojo [params = " + params + "]";
    }
}
