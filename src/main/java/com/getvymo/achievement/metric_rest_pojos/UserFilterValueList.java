package com.getvymo.achievement.metric_rest_pojos;

import java.util.Arrays;

/**
 * Created by abhishek on 10/04/17.
 */
public class UserFilterValueList implements IUserFilterValue {
    String[] list;
    private String userType;

    public UserFilterValueList(String[] list, String userType) {
        this.list = list;
        this.userType = userType;
    }

    public String[] getList() {
        return list;
    }

    public void setList(String[] list) {
        this.list = list;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "UserFilterValueList{" +
                "list=" + Arrays.toString(list) +
                ", userType='" + userType + '\'' +
                '}';
    }
}
