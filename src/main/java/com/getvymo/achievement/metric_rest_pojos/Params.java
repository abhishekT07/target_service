package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class Params {
    private String[] groupBy;

    private String[] metricIds;

    private Filters[] filters;

    private String clientId;

    public Params() {
    }

    public Params(String[] groupBy, String[] metricIds, Filters[] filters, String clientId) {
        this.groupBy = groupBy;
        this.metricIds = metricIds;
        this.filters = filters;
        this.clientId = clientId;
    }

    public String[] getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String[] groupBy) {
        this.groupBy = groupBy;
    }

    public String[] getMetricIds() {
        return metricIds;
    }

    public void setMetricIds(String[] metricIds) {
        this.metricIds = metricIds;
    }

    public Filters[] getFilters() {
        return filters;
    }

    public void setFilters(Filters[] filters) {
        this.filters = filters;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "ClassPojo [groupBy = " + groupBy + ", metricIds = " + metricIds + ", filters = " + filters + ", clientId = " + clientId + "]";
    }
}
