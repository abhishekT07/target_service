package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class UserFilterValue implements IUserFilterValue {
    private String userId;

    private String userType;

    public UserFilterValue(String userId, String userType) {
        this.userId = userId;
        this.userType = userType;
    }

    public UserFilterValue() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "ClassPojo [userId = " + userId + ", userType = " + userType + "]";
    }
}
