package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class DateFilterValue {
    private String start;
    private String end;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public DateFilterValue() {
    }

    public DateFilterValue(String start, String end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "DateFilterValue{" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
