package com.getvymo.achievement.metric_rest_pojos;

/**
 * Created by abhishek on 25/03/17.
 */
public class DimensionIdMap {
    private String activity;
    private String sub_activity;

    public DimensionIdMap(String activity, String sub_activity) {
        this.activity = activity;
        this.sub_activity = sub_activity;
    }

    public DimensionIdMap() {
    }

    public String getSub_activity() {
        return sub_activity;
    }

    public void setSub_activity(String sub_activity) {
        this.sub_activity = sub_activity;
    }

    public String getActivity() {

        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "ClassPojo [activity = " + activity + "]";
    }
}
