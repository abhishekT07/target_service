package com.getvymo.achievement;

import com.getvymo.data.response.entities.AchievementUserEntity;
import com.getvymo.data.sql.repositories.target_repos.FactsMetricsRepository;
import com.getvymo.data.sql.repositories.target_repos.FactsRepository;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * Created by abhishek on 25/03/17.
 */
public interface GetAchievement {
    public float getAchievement(int metric_fact_id, String userCode, String self, String start, String end, String
            clientCode, String fact_id, List<AchievementUserEntity> achievementUserEntityList);
    public JSONObject getAchievement(int metric_fact_id, String start, String end, String
            clientCode, List<AchievementUserEntity> achievementUserEntityList);
}
