package com.getvymo.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by abhishek on 21/03/17.
 */

@Configuration
@PropertySource("classpath:application.properties")
@EnableJpaRepositories(
        entityManagerFactoryRef = "dsTargetsEntityManagerFactory",
        transactionManagerRef = "dsTargetsTransactionManager",
        basePackages = {"com.getvymo.data.sql.repositories.target_repos"}
)
public class TargetServiceSqlConfig extends DataSourceProperties {

    private static final String PROPERTY_NAME_DATABASE_DRIVER = "spring.datasource.driver-class-name";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD = "spring.primary.password";
    private static final String PROPERTY_NAME_DATABASE_URL = "spring.primary.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME = "spring.primary.username";

    private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "spring.jpa.properties.hibernate.dialect";
    private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "spring.jpa.show-sql";

    @Resource
    private Environment env;

    @Bean(name = "dsTargets")
    @Primary
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
        dataSource.setUrl(env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
        dataSource.setUsername(env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
        dataSource.setPassword(env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));

        return dataSource;
    }


    @PersistenceContext(unitName = "primary")
    @Bean(name = "dsTargetsEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean targetEntityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource());
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);
        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactoryBean.setPackagesToScan("com.getvymo.data.sql.entities.target_entities");

        entityManagerFactoryBean.setJpaProperties(hibProperties());

        return entityManagerFactoryBean;
    }


    private Properties hibProperties() {
        Properties properties = new Properties();

        properties.put("hibernate.show_sql", env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        properties.put("hibernate.dialect", env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
        properties.put("hibernate.hbm2ddl.auto", "validate");
        return properties;
    }

    @Bean(name = "dsTargetsTransactionManager")
    @Primary
    public PlatformTransactionManager transactionManager(
            @Qualifier("dsTargetsEntityManagerFactory") EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(targetEntityManagerFactory().getObject());
        return transactionManager;
    }
}
