package com.getvymo.kafka.data;

import java.util.Date;

/**
 * Created by abhishek on 24/04/17.
 */
public class TaskAttribute {
    private String clientId;
    private String hierarchyKey;
    private String userId;
    private String type;
    private String eventId;
    private String date;

    public TaskAttribute() {
    }

    public TaskAttribute(String clientId, String hierarchyKey, String userId, String type, String eventId, String date) {
        this.clientId = clientId;
        this.hierarchyKey = hierarchyKey;
        this.userId = userId;
        this.type = type;
        this.eventId = eventId;
        this.date = date;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "TaskAttribute{" +
                "clientId='" + clientId + '\'' +
                ", hierarchyKey='" + hierarchyKey + '\'' +
                ", userId='" + userId + '\'' +
                ", type='" + type + '\'' +
                ", eventId='" + eventId + '\'' +
                ", date=" + date +
                '}';
    }
}
