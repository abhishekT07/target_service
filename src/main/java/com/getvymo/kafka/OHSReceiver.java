package com.getvymo.kafka;

import com.getvymo.cache.IDateKeyCache;
import com.getvymo.data.response.entities.ActiveTargets;
import com.getvymo.data.sql.entities.org_hierarchy_entities.UserVsRoles;
import com.getvymo.data.sql.entities.org_hierarchy_entities.Users;
import com.getvymo.data.sql.entities.target_entities.TargetInstance;
import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
import com.getvymo.data.sql.entities.target_entities.UserVsTargetInstance;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UserVsRolesRepository;
import com.getvymo.data.sql.repositories.org_hierarchy_repos.UsersRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetInstanceRepository;
import com.getvymo.data.sql.repositories.target_repos.TargetRespository;
import com.getvymo.data.sql.repositories.target_repos.UserVsTargetInstanceRepository;
import com.getvymo.kafka.data.TaskAttribute;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abhishek on 24/04/17.
 */
@Component
public class OHSReceiver {
    private static Logger logger = LoggerFactory.getLogger(OHSReceiver.class);
    @Autowired
    private TargetRespository targetRespository;
    @Autowired
    private UserVsTargetInstanceRepository userVsTargetInstanceRepository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UserVsRolesRepository userVsRolesRepository;
    @Autowired
    private TargetInstanceRepository targetInstanceRepository;
    @Autowired
    private IDateKeyCache iDateKeyCache;

    @Autowired
    JdbcTemplate jdbcTemplate;



    @KafkaListener(topics = "${kafka.receiver.topic-ohs}", containerFactory = "kafkaListenerContainerFactory", group = "${spring.kafka.consumer.group-id}")
    public void listen(@Payload(required = false) TaskAttribute taskAttribute) throws Exception {
        if (taskAttribute != null && taskAttribute.getType().equals("managerChanged")) {
            logger.info("MAnager change event received+++++++");
            String userCode = taskAttribute.getUserId();
            String clientCode = taskAttribute.getClientId();
            String newHierarchyKey = taskAttribute.getHierarchyKey();
            if (!(userCode.isEmpty() && clientCode.isEmpty() && newHierarchyKey.isEmpty())) {
                logger.info("Received proper data in manager change kafka event");
                List<String> dateKeys = iDateKeyCache.getFutureDateKeys(clientCode);
                if (dateKeys!=null){
                    updateOnManagerChange(userCode,clientCode,dateKeys);
                }
//                targetRespository.updateUVTOnManagerChange(userCode, clientCode, s);
            } else {
                logger.warn("Empty data found in kafka event" + taskAttribute.toString());
            }

        } else if (taskAttribute != null && taskAttribute.getType().equals("newUser")) {
            String recvdUserCode = taskAttribute.getUserId();
            String recvdClientCode = taskAttribute.getClientId();
            String newHierarchyKey = taskAttribute.getHierarchyKey();
            if (!(recvdUserCode.isEmpty() && recvdClientCode.isEmpty() && newHierarchyKey.isEmpty())) {
                logger.info("Received proper data in new user creation kafka event" + taskAttribute.toString());
                processNewUser(recvdUserCode, recvdClientCode, newHierarchyKey);
            } else {
                logger.warn("Empty data found in kafka event" + taskAttribute.toString());
            }
        } else if (taskAttribute != null && taskAttribute.getType().equals("roleChanged")) {
            String recvdUserCode = taskAttribute.getUserId();
            String recvdClientCode = taskAttribute.getClientId();
            String newHierarchyKey = taskAttribute.getHierarchyKey();
            //For role change we are assigning the user new hierarchy key just that for now
            if (!(recvdUserCode.isEmpty() && recvdClientCode.isEmpty() && newHierarchyKey.isEmpty())) {
                logger.info("Received proper roleChange event from kafka for " + recvdUserCode);
                targetRespository.updateHKeyOnRoleChange(recvdUserCode, recvdClientCode, newHierarchyKey);
            }
        } else if (taskAttribute != null && taskAttribute.getType().equals("disableUser")) {
            String recvdUserCode = taskAttribute.getUserId();
            String recvdClientCode = taskAttribute.getClientId();
            //We will be disabling all the live and future targets for this user
            if (!(recvdUserCode.isEmpty() && recvdClientCode.isEmpty() )) {
                logger.info("Received proper disable user event from kafka for " + recvdUserCode);
                List<String> dateKeys = iDateKeyCache.getFutureDateKeys(recvdClientCode);
//                String s = getInQueryForFutureDates(dateKeys);
                //If date keys present => target enabled for this client
                if (dateKeys!=null){
                    changeUserVisibility(dateKeys,recvdUserCode,recvdClientCode,1);
                }
//                targetRespository.changeUserTargetVisibility(recvdUserCode, recvdClientCode, 1, s);
            }
        } else if (taskAttribute != null && taskAttribute.getType().equals("enableUser")) {
            String recvdUserCode = taskAttribute.getUserId();
            String recvdClientCode = taskAttribute.getClientId();
            //We will be enabling all the live and future targets for this user
            if (!(recvdUserCode.isEmpty() && recvdClientCode.isEmpty())) {
                logger.info("Received proper disable user event from kafka for " + recvdUserCode);
                List<String> dateKeys = iDateKeyCache.getFutureDateKeys(recvdClientCode);
                if (dateKeys!=null){
                    changeUserVisibility(dateKeys,recvdUserCode,recvdClientCode,0);
                }
//                String s = getInQueryForFutureDates(dateKeys);
//                targetRespository.changeUserTargetVisibility(recvdUserCode, recvdClientCode, 0, s);
            }
        }
    }

    public void processNewUser(String recvdUserCode, String recvdClientCode, String newHierarchyKey) {

        List<UserVsTargetInstance> userVsTargetInstanceList = userVsTargetInstanceRepository.findByClientCode(recvdClientCode);
        if (!userVsTargetInstanceList.isEmpty()) {
            for (UserVsTargetInstance userVsTargetInstance : userVsTargetInstanceList) {
                String savedUser = userVsTargetInstance.getUserCode();
                String savedClientCode = userVsTargetInstance.getClientCode();
                String savedRole = userVsTargetInstance.getRole();
                String savedAssignmentType = userVsTargetInstance.getAssignmentType();
                Users user = null;
                UserVsRoles userVsRoles = null;
                if (!savedRole.isEmpty() && savedRole.equals("NA")) {
                    switch (savedAssignmentType) {
                        case "self":
                            //Not applicable as user can't have userVsTargetInstance before user is even in system
                            break;
                        case "all_hierarchy":
                            user = usersRepository.getNewUserForAllHierarchy(userVsTargetInstance.getUserCode(), userVsTargetInstance.getClientCode(), recvdUserCode);
                            break;
                        case "direct_hierarchy":
                            user = usersRepository.getNewUserForDirectHierarchy(userVsTargetInstance.getUserCode(), userVsTargetInstance.getClientCode(), recvdUserCode);
                            break;
                    }
                    if (user != null) {
                        TargetInstance targetInstance = targetInstanceRepository.findById(userVsTargetInstance.getTargetInstanceId());
                        UserVsTarget userVsTarget = new UserVsTarget(user.getUserId(), user.getHierarchyKey(),
                                userVsTargetInstance.getTargetInstanceId(), targetInstance.getMetricFactId(),
                                targetInstance.getDateKey(), user.getClientId(), userVsTargetInstance.getTargetValue(), 0);
                        logger.info("Saving target for new user " + recvdUserCode + " label->" + targetInstance.getLabel());
                        targetRespository.save(userVsTarget);
                    }
                } else if (!savedRole.isEmpty()) {
                    userVsRoles = userVsRolesRepository.getNewUserUsersWithRole(savedUser, savedClientCode, savedRole, recvdUserCode);
                    if (userVsRoles != null) {
                        TargetInstance targetInstance = targetInstanceRepository.findById(userVsTargetInstance.getTargetInstanceId());
                        UserVsTarget userVsTarget = new UserVsTarget(userVsRoles.getUserCode(),
                                userVsRoles.getHierarchyKey(), userVsTargetInstance.getTargetInstanceId(),
                                targetInstance.getMetricFactId(), targetInstance.getDateKey(),
                                userVsRoles.getClientCode(), userVsTargetInstance.getTargetValue(), 0);
                        logger.info("Saving target for new user " + recvdUserCode + " label->" + targetInstance.getLabel());
                        targetRespository.save(userVsTarget);
                    }
                }
            }
        }
        logger.info("Successfully processed new user creation event for userCode " + recvdClientCode + recvdUserCode);
    }

    private String getInQueryFromList(List<String> list){
        String temp = "(";
        for (String values : list) {
            temp += "?,";
        }
        temp += ")";
        temp = temp.replace(",)", ")");
        return temp;
    }

    private void updateOnManagerChange(String userCode, String clientCode, List<String> dateKeys){
        String query = "update targets_service.user_vs_target uvt join org_hierarchy_service.users usr on " +
        "usr.userId=uvt.user_code join org_hierarchy_service.usersVsAccessibleHierarchyKeys uvahk on " +
                "uvahk.hierarchyKey=usr.hierarchyKey set uvt.hierarchy_key= usr.hierarchyKey where  uvahk.userId='%s' " +
                "and uvahk.clientId='%s' and uvt.client_code=uvahk.clientId and usr.clientId=uvahk.clientId and uvt.date_key in %s";
        Object[] params = dateKeys.toArray();
        String temp = getInQueryFromList(dateKeys);
        String updateQuery = String.format(query,userCode,clientCode,temp);
        List<Integer> argTypes = new ArrayList<>(params.length);
        for (int i=0; i<params.length;i++){
            argTypes.add(Types.VARCHAR);
        }
        int[] argArray = ArrayUtils.toPrimitive(argTypes.toArray(new Integer[argTypes.size()]));
        jdbcTemplate.update(updateQuery, params, argArray);
    }

    private void changeUserVisibility(List<String> dateKeys, String userCode, String clientCode, int disabled){
        String query = "update targets_service.user_vs_target set disabled= '%s' where user_code= '%s' and " +
                "client_code= '%s' and date_key in  %s";
        Object[] params = dateKeys.toArray();
        String temp = getInQueryFromList(dateKeys);
        String updateQuery = String.format(query,disabled,userCode,clientCode,temp);
        List<Integer> argTypes = new ArrayList<>(params.length);
        for (int i=0; i<params.length;i++){
            argTypes.add(Types.VARCHAR);
        }
        int[] argArray = ArrayUtils.toPrimitive(argTypes.toArray(new Integer[argTypes.size()]));
        jdbcTemplate.update(updateQuery, params, argArray);
    }

    /*public String getInQueryForFutureDates(List<String> dateKeys) {
        String s = new String();
        for (String dateKey : dateKeys) {
            if (s.length() == 0) {
                s += "'" + dateKey + "'";
            } else {
                s += ", '" + dateKey + "'";
            }

        }
        return s;
    }*/
}
