package com.getvymo.kafka.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getvymo.kafka.data.TaskAttribute;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Created by abhishek on 24/04/17.
 */
public class TaskAttributeUnmarshall implements Deserializer<TaskAttribute>{
    Logger logger = LoggerFactory.getLogger(TaskAttributeUnmarshall.class);
    ObjectMapper objectMapper = new ObjectMapper();
    /**
     * Configure this class.
     *
     * @param configs configs in key/value pairs
     * @param isKey   whether is for key or value
     */
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    /**
     * Deserialize a record value from a bytearray into a value or object.
     *
     * @param topic topic associated with the data
     * @param data  serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
     * @return deserialized typed data; may be null
     */
    @Override
    public TaskAttribute deserialize(String topic, byte[] data) {
        if (data == null){
            return null;
        } else {
            try {
                return objectMapper.readValue(data, TaskAttribute.class);
            } catch (IOException e){
                logger.info("Unable to deserialize kafka event "+e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void close() {

    }
}
