//package com.getvymo;
//
//import org.junit.*;
// import org.junit.runner.*;
// import org.springframework.boot.test.autoconfigure.orm.jpa.*;
// 
// import static org.assertj.core.api.Assertions.*;
// import com.getvymo.config.OrgHierarchySqlConfig;
// import com.getvymo.config.TargetServiceSqlConfig;
// import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
// import com.getvymo.data.sql.repositories.target_repos.TargetRespository;
// import org.springframework.test.context.junit4.SpringRunner;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.test.context.ContextConfiguration;
// import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
// import org.springframework.transaction.annotation.EnableTransactionManagement;
// import org.springframework.transaction.annotation.Transactional;
// import org.springframework.boot.test.SpringApplicationConfiguration;
// import org.springframework.boot.autoconfigure.SpringBootApplication;
// 
// @RunWith(SpringJUnit4ClassRunner.class)
// //@ComponentScan(basePackages = { "com.getvymo.config" })
// @ContextConfiguration(classes = { TargetServiceSqlConfig.class, OrgHierarchySqlConfig.class })
// @SpringApplicationConfiguration(classes = UserVsTarget.class)
// @EnableTransactionManagement
// public class UserRepositoryTest {
// 
//      @Autowired
//      private TestEntityManager entityManager;
//
//     @Autowired
//     private TargetRespository repository;
// 
//     @Test
//    
//     public void testExample() throws Exception {
//     	
//     	
//     	UserVsTarget user = new UserVsTarget();
// 		user.setUserCode("Test02");
// 		user.setDateKey("w-30-37");
// 		user.setHierarchyKey("TestHierarchy03");
// 		user.setInstanceId("Test02");
// 		user.setMetricFactId("Test02");
// 		user = repository.save(user);
// 		final UserVsTarget result = repository.findByUserCode(user.getUserCode()).get(0);
// 		assertThat(user.getUserCode()).isEqualTo("Test02");
//     	
//        /*  this.entityManager.persist(new User("sboot", "1234"));
//         
//          User user = this.repository.findByUsername("sboot");
//          assertThat(user.getUsername()).isEqualTo("sboot");
//          assertThat(user.getVin()).isEqualTo("1234");*/
//     }
// 
// }