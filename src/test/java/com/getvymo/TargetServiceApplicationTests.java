package com.getvymo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.getvymo.data.sql.entities.target_entities.UserVsTarget;
import org.springframework.beans.factory.annotation.*;
import com.getvymo.data.sql.repositories.target_repos.TargetRespository;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import static org.hamcrest.Matchers.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest

public class TargetServiceApplicationTests {
	@Autowired
	TargetRespository userVsTargetRepository;
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private TargetRespository  targetRespository;

	@Test
	public void contextLoads() {
	}

	// Test case for checking the result set
	@Test

	public void whenQueryFired() {

		UserVsTarget userVsTarget = new UserVsTarget();
		userVsTarget.setUserCode("test01");
		userVsTarget.setDateKey("w-30-37");
		userVsTarget.setHierarchyKey("TestHierarchy03");
		userVsTarget.setInstanceId(1);
		userVsTarget.setMetricFactId(02);
		userVsTarget.setClientCode("sbi_banca");
		userVsTarget.setValue(10);
		UserVsTarget newUserVsTarget = userVsTargetRepository.save(userVsTarget);
		System.out.println("UVT SAVED"+newUserVsTarget.toString());
		final UserVsTarget result = userVsTargetRepository.findByUserCode(userVsTarget.getUserCode()).get(0);
		System.out.println("UVT SAVED"+result.toString());
//		assertNotNull(result);
		assertEquals(result.getUserCode(),"test01");

		}



	// Test case for JPQL and CrudRepository Comparison
	/*@Test
	public void whenCreatingUser_thenCreatedByJPQL() {

		UserVsTarget userVsTarget = new UserVsTarget();
		userVsTarget.setUserCode("Test03");
		userVsTarget.setDateKey("w-30-37");
		userVsTarget.setHierarchyKey("TestHierarchy03");
		userVsTarget.setInstanceId(02);
		userVsTarget.setMetricFactId(03);
		userVsTarget.setValue(50);
		userVsTarget.setClientCode("Sbi");
		userVsTarget = userVsTargetRepository.save(userVsTarget);
		
		final UserVsTarget result = userVsTargetRepository.findByUserCodeAndInstanceId("test01",1);
		String query = "select user_code,instance_id,metric_fact_id,hierarchy_key,date_key,value,client_code From targets_service.user_vs_target where user_code='test01' and instance_id=1";

		List<Object[]> results = entityManager.createNativeQuery(query).getResultList();
		System.out.println("class" + results.getClass());
		UserVsTarget userVsTarget1 = new UserVsTarget();
		for (Object[] usersVsTargets : results) {

			userVsTarget1.setUserCode((String) usersVsTargets[0]);
			userVsTarget1.setInstanceId((int) usersVsTargets[1]);
			userVsTarget1.setHierarchyKey((String) usersVsTargets[3]);
			userVsTarget1.setMetricFactId((int) usersVsTargets[2]);
			userVsTarget1.setDateKey((String)usersVsTargets[4]);
			userVsTarget1.setValue((int)usersVsTargets[5]);
			userVsTarget1.setClientCode((String)usersVsTargets[6]);
			
		}
		assertEquals(result, userVsTarget1);
	}*/

	 // Test case to check the result set with user code and instance id given
	@Test
	public void whenSearchAnUser_theResult() {

		UserVsTarget userVsTarget = new UserVsTarget();
		userVsTarget.setUserCode("test4");
		userVsTarget.setDateKey("w-30-37");
		userVsTarget.setHierarchyKey("TestHierarchy04");
		userVsTarget.setInstanceId(12);
		userVsTarget.setMetricFactId(03);
		userVsTarget.setClientCode("sbi_banca");
		userVsTarget.setValue(20);
		userVsTarget = userVsTargetRepository.save(userVsTarget);
		final UserVsTarget result = userVsTargetRepository.findByUserCodeAndInstanceId(userVsTarget.getUserCode(),
				userVsTarget.getInstanceId());
		assertEquals(result, userVsTarget);

	}

}
